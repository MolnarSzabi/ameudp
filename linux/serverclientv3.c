#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <sched.h>
#include <linux/sched.h>
#include <time.h>
#include <pthread.h>
#include <curses.h>
#include <enet/enet.h>

#define SRC_PORT 992   //en - sender es 991 a reciever 
#define DST_PORT_MOOG 991
#define RECV_PORT 980     //sender regebb 100
#define RECV_SRC_PORT 981  //en - recievercregebb 101

pthread_mutex_t lock; 
 typedef float data;
//typedef _Float data;
	
ENetAddress enetAddr;
ENetHost* enetServer;
ENetEvent  event;
ENetPacket* packet;
    	
bool running = true, recieved = false;
int sock, recvsock, n, threadNr = 0;
unsigned long long int k = 0, k_recv = 0, counter = 0;
char out = 'c';
unsigned int length, recv_length;
struct sockaddr_in server, from,  srcaddr , recvaddr;
struct hostent *hp;
struct hostent *rp;						//global variables for thread
   
data buffer3[8] =  {1.821688e-43,0.01,0.01,-0.01,0.01,0.01,0.01,0};
data wat[8] =  {1.821688e-43,0.01,0.01,-0.01,0.01,0.01,0.01,0};

pthread_t thread_id,thread_id_send, thread_id_recv, thread_id_recv_enet, thread_id_print;  //thread id

void error(const char *);

float float_swap(float value){
       int temp =  htonl(*(unsigned int*)&value);
       return *(float*)&temp;
};

void *printInfo(void *vargp) 
{
	 initscr();
     cbreak();
     noecho();           //required for curses.h
     nonl();
     intrflush(stdscr, FALSE);
     keypad(stdscr, TRUE);
	 data printBuf[8];
	 memcpy(printBuf,buffer3, sizeof(buffer3[0]*8));
	 while(out != 'q')
	 {
		 
		printw("Sending msg %lli\n", k);
		printw("Recieved msg %lli\n", k_recv);
			
			
		if(recieved == true)
		{
			for(int i = 0; i < 8; i++)
				printBuf[i] = float_swap(buffer3[i]); 
				recieved = false;
			}
			
			for (size_t i = 0; i < 8; i++)
				printw("%.10f ",printBuf[i]);
			
			printw("\n");
			usleep(8000);
			
			refresh();
			clear();
	 }
	 endwin();
	 pthread_exit(NULL);
	
}
void *myThreadFun(void *vargp) 
{ 
 pthread_detach(thread_id_recv);
 n = recvfrom(recvsock,(char*)&wat,sizeof(wat),0,(struct sockaddr *)&from, &recv_length);
 if (n < 0) error("recvfrom");
   
	pthread_mutex_lock(&lock); 
	
	memcpy(buffer3, wat, sizeof(wat[0])*8); 
	recieved = true;	
	for(int i = 0; i < 8; i++)
		buffer3[i] = float_swap(buffer3[i]); 
	
	pthread_mutex_unlock(&lock);  
	k_recv++;				
    int rx_thread = pthread_create(&thread_id_recv, NULL, myThreadFun, NULL);
    //if(rx_thread!=0)
		//printf("rx_thread:%d\n", rx_thread);
     
   pthread_exit(NULL);
} 

void *threadReceiveEnet(void *vargp)
{
	//pthread_detach(thread_id_recv_enet);
	if (enet_initialize() != 0) {
			 printf("could not initialize enet.");
			 return 0;
		 }
		 
	//enet_address_set_host(&enetAddr, "127.0.0.1"/*argv[2]*/);
	// enetAddr.port = RECV_SRC_PORT;
	
	enetAddr.host = ENET_HOST_ANY;
	enetAddr.port = RECV_SRC_PORT;
		
	enetServer = enet_host_create(&enetAddr, 1, 0, 0, 0);

	if (enetServer == NULL) {
		printf("could not start server.\n");
		return 0;
	}
		 
	while (running) {
		if (enet_host_service(enetServer, &event, 0) > 0) {
			switch (event.type) {
			case ENET_EVENT_TYPE_CONNECT:
			//printf("Connected!\n");
				break;
				
			case ENET_EVENT_TYPE_RECEIVE:
				if (&enetServer->peers[1] != event.peer) {

					recieved = true;
					pthread_mutex_lock(&lock); 
					 memcpy(buffer3, (data*)event.packet->data, sizeof(data)*8); 
					
					 for(int i = 0; i < 8; i++)
						buffer3[i] = float_swap(buffer3[i]); 
					pthread_mutex_unlock(&lock);  
					k_recv++;				
				}
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				//std::cout << "Peer has disconnected!" << std::endl;
			//printf("Peer has disconnected!\n");
				free(event.peer->data);
				event.peer->data = NULL;
				running = false;
				
				close(sock);
				enet_host_destroy(enetServer);
				enet_deinitialize(); 
				endwin();
				exit(0);
				
				break;

			case ENET_EVENT_TYPE_NONE:
				//printf("Tick tock.\n");
				break;

			default:
			//	printf("Tick tock.\n");
				break;
			}

		}
	}
	 //pthread_exit(NULL);
}

void *ThreadChar(void *vargp) 
{ 
  //pthread_detach(thread_id);
  while(out!= 'q'){
     // if (n < 0) error("Sendto");
   out = getch();
  }
  //pthread_exit(NULL);
} 

void *myThreadSend(void *vargp)
{
	
	pthread_detach(thread_id_send);
	initscr();
 //   cbreak();
   // noecho();           //required for curses.h
   // nonl();
    //intrflush(stdscr, FALSE);
   // keypad(stdscr, TRUE);
    while(out != 'q')
	{
	  pthread_mutex_lock(&lock); 
	  n = sendto(sock,(char*)&buffer3, sizeof(buffer3),0  ,(const struct sockaddr *)&server,length);
	  pthread_mutex_unlock(&lock);  
	  //usleep(16666);  //1/60 = 0.01666 = 16666 in microseconds
	  usleep(8888);
	  k++;	      
	}
	//clear();
	//refresh();
	 endwin();
	 pthread_exit(NULL);
}

int main(int argc, char *argv[]){

   printf("Starting program!\n");
	
	if (pthread_mutex_init(&lock, NULL) != 0) 
	{ 
		printf("\n mutex init has failed\n"); 
		return 1; 
	} 
	
    sock = socket(AF_INET, SOCK_DGRAM, 0);             //CLIENT SOCKET
    if (sock < 0) error("socket");

    server.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);                         //CLIENT IP   - kinek kuldom?
    if (hp==0) error("Unknown host");
  
    bcopy((char *)hp->h_addr, 
        (char *)&server.sin_addr,
         hp->h_length);
		 		 
		 
    server.sin_port = htons(DST_PORT_MOOG);    //SERVER PORT         - melyik portra?
    length=sizeof(struct sockaddr_in);          //SERVER LENGTH

    memset(&srcaddr, 0, sizeof(srcaddr));
    srcaddr.sin_family = AF_INET;
    srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //SERVER SRCPORT
    srcaddr.sin_port = htons(SRC_PORT);            
	
    if (bind(sock, (struct sockaddr *) &srcaddr, sizeof(srcaddr)) < 0) {
        perror("bind");                                          //BIND FOR SRCPORT
        exit(1);
    }
    printf("Sending Bind ok!\n");
    

	if(strcmp(argv[3],"winsock") == 0)
	{
		recvsock = socket(AF_INET, SOCK_DGRAM, 0);         //RECV SOCKET
		if (recvsock < 0) error("socket");

		from.sin_family = AF_INET;
		rp = gethostbyname(argv[2]);                         //RECV IP  - az en IP-m
		if (rp==0) error("Unknown reciever");

		bcopy((char *)rp->h_addr, 
			(char *)&from.sin_addr,
			 rp->h_length);
		from.sin_port = htons(RECV_PORT);    //RECIVE PORT
		recv_length=sizeof(struct sockaddr_in);          //RECIVE LENGTH

		memset(&recvaddr, 0, sizeof(recvaddr));
		recvaddr.sin_family = AF_INET;
		recvaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //RECIVE SRCPORT
		recvaddr.sin_port = htons(RECV_SRC_PORT);     

		if (bind(recvsock, (struct sockaddr *) &recvaddr, sizeof(recvaddr)) < 0) {
			perror("bind");                                          //BIND FOR SRCPORT
			exit(1);
		}
		printf("Recieving Bind ok!\n");
}

   _Float32 buffer[8] =  {1.821688e-43,0,0,0,0,0,0,0};   //start 
   
   for(int i = 0; i < 8; i++)
      buffer[i] = float_swap(buffer[i]);      //change endian

   printf("Sending start data!\n");

  for(int i = 0; i<5; i++) {
     n = sendto(sock,(char*)&buffer, sizeof(buffer),0,(const struct sockaddr *)&server,length);
     if (n < 0)                                   //sending start 5 times
  	error("Sendto");
    
   usleep(16666.66);
  }

  _Float32 buf[8] = {2.45e-43,0,0,0,0,0,0,0};
  
  for(int i = 0; i < 8; i++)
     buf[i] = float_swap(buf[i]);     //change endian

  printf("Sending data 1!\n");
  n = sendto(sock,(char*)&buf, sizeof(buf),0 ,(const struct sockaddr *)&server,length);  //sending data
  
  for(int i = 0; i < 8; i++)
    buffer3[i] = float_swap(buffer3[i]);    //change endian

 
   printf("Waiting for data!\n");
   
    initscr();		//required for exit
    
    pthread_create(&thread_id, NULL, ThreadChar, NULL);  //call thread
    
	if(strcmp(argv[3],"winsock") == 0)
	{
	pthread_create(&thread_id_recv, NULL, myThreadFun, NULL);
	}
	else{
	pthread_create(&thread_id_recv_enet, NULL, threadReceiveEnet, NULL);
    }

	pthread_create(&thread_id_print, NULL, printInfo, NULL);  //call thread
    
    pthread_create(&thread_id_send, NULL, myThreadSend, NULL);   //send msg
    pthread_join(thread_id_send, NULL);

   
   _Float32 buffer4[8] = {2.94e-43,0,0,0,0,0,0,0};
  
   for(int i = 0; i < 8; i++)
     buf[i] = float_swap(buffer4[i]);     //change endian
  
   

   printf("Sent %lli messages!\n", k);
   printf("Parking!\n");
   n = sendto(sock,(char*)&buf, sizeof(buf),0,(const struct sockaddr *)&server,length);  //sending data
  
   printf("Finish! \n");
   close(sock);
   enet_host_destroy(enetServer);
   enet_deinitialize(); 
   pthread_mutex_destroy(&lock); 
   
   endwin();
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}
