#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sched.h>
#include <linux/sched.h>
#include <time.h>
#include <pthread.h>
#include <curses.h>


#define SRC_PORT 992   //en - sender es 991 a reciever 
#define RECV_PORT 980      //sender regebb 100
#define RECV_SRC_PORT 981  //en - recievercregebb 101

//#define RECV_IP "128.127.55.121"

    int sock, recvsock, n, threadNr = 0;
    unsigned long long int k = 0, k_recv = 0;
    char out;
    unsigned int length, recv_length;
    struct sockaddr_in server, from,  srcaddr , recvaddr;
    struct hostent *hp;
    struct hostent *rp;						//global variables for thread
   
   _Float32 buffer3[8] =  {1.821688e-43,0,0,-0.01,0,0,0,0};

   pthread_t thread_id,thread_id_send, thread_id_recv;  //thread id

void error(const char *);

float float_swap(float value){
       int temp =  htonl(*(unsigned int*)&value);
       return *(float*)&temp;
  // Y = swapbytes(f); in matlab
};

void *myThreadFun(void *vargp) 
{
    
   n = recvfrom(recvsock,(char*)&buffer3,sizeof(buffer3),0,(struct sockaddr *)&from, &recv_length);
   if (n < 0) error("recvfrom");
   k_recv++;

   for(int i = 0; i < 8; i++)
      buffer3[i] = float_swap(buffer3[i]); 

   int rx_thread = pthread_create(&thread_id_recv, NULL, myThreadFun, NULL);
   if(rx_thread!=0)
   	printf("rx_thread:%d\n", rx_thread);
    
   pthread_detach(thread_id_recv);
   pthread_exit(NULL);
} 
void *ThreadChar(void *vargp) 
{ 
  while(out!= 'q'){
      if (n < 0) error("Sendto");
   out = getch();
  }
} 

void *myThreadSend(void *vargp)
{
      k++;	
      n = sendto(sock,(char*)&buffer3, sizeof(buffer3),0,(const struct sockaddr *)&server,length);
//      usleep(8000);  //1/60 = 0.1666 = 16.6 in milliseconds
      
}

int main(int argc, char *argv[]){
 
    printf("Starting program!\n");

    sock = socket(AF_INET, SOCK_DGRAM, 0);             //CLIENT SOCKET
    if (sock < 0) error("socket");

    server.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);                         //CLIENT IP   - kinek kuldom?
    if (hp==0) error("Unknown host");
  
    bcopy((char *)hp->h_addr, 
        (char *)&server.sin_addr,
         hp->h_length);
    server.sin_port = htons(atoi(argv[2]));    //SERVER PORT         - melyik portra?
    length=sizeof(struct sockaddr_in);          //SERVER LENGTH

    memset(&srcaddr, 0, sizeof(srcaddr));
    srcaddr.sin_family = AF_INET;
    srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //SERVER SRCPORT
    srcaddr.sin_port = htons(SRC_PORT);            
	
    if (bind(sock, (struct sockaddr *) &srcaddr, sizeof(srcaddr)) < 0) {
        perror("bind");                                          //BIND FOR SRCPORT
        exit(1);
    }
    printf("Sending Bind ok!\n");
    

    recvsock = socket(AF_INET, SOCK_DGRAM, 0);         //RECV SOCKET
    if (recvsock < 0) error("socket");

    from.sin_family = AF_INET;
    rp = gethostbyname(argv[3]);                         //RECV IP  - az en IP-m
    if (rp==0) error("Unknown reciever");

    bcopy((char *)rp->h_addr, 
        (char *)&from.sin_addr,
         rp->h_length);
    from.sin_port = htons(RECV_PORT);    //RECIVE PORT
    recv_length=sizeof(struct sockaddr_in);          //RECIVE LENGTH

    memset(&recvaddr, 0, sizeof(recvaddr));
    recvaddr.sin_family = AF_INET;
    recvaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //RECIVE SRCPORT
    recvaddr.sin_port = htons(RECV_SRC_PORT);     

    if (bind(recvsock, (struct sockaddr *) &recvaddr, sizeof(recvaddr)) < 0) {
        perror("bind");                                          //BIND FOR SRCPORT
        exit(1);
    }
    printf("Recieving Bind ok!\n");


   _Float32 buffer[8] =  {1.821688e-43,0,0,0,0,0,0,0};   //start 
   
   for(int i = 0; i < 8; i++)
      buffer[i] = float_swap(buffer[i]);      //change endian

   printf("Sending start data!\n");

  for(int i = 0; i<5; i++) {
     n = sendto(sock,(char*)&buffer, sizeof(buffer),0,(const struct sockaddr *)&server,length);
     if (n < 0)                                   //sending start 5 times
  	error("Sendto");
    
   usleep(16666.66);
  }

  _Float32 buf[8] = {2.45e-43,0,0,0,0,0,0,0};
  
  for(int i = 0; i < 8; i++)
     buf[i] = float_swap(buf[i]);     //change endian

  printf("Sending data 1!\n");
  n = sendto(sock,(char*)&buf, sizeof(buf),0,(const struct sockaddr *)&server,length);  //sending data
  
     _Float32 buf5sec[8] = {1.821688e-43,0,0,-0.01,0,0,0,0};    
     
    for(int i = 0; i < 8; i++)
         buf5sec[i] = float_swap(buf5sec[i]);    //change endian
    
    printf("Wait 5 seconds\n");
    for(int i = 0; i< 600; i++)
    {     
	 
         n = sendto(sock,(char*)&buf5sec, sizeof(buf5sec),0,(const struct sockaddr *)&server,length);
         if (n < 0) error("Sendto");              //send data

         usleep(8000);
    } 
  
   
   _Float32 bufferHeave[8] =  {1.821688e-43,0,0,-0.01,0,0,0,0};
   printf("Setting position of heave to -0.02\n");
   
  for(int i = 0; i< 3800; i++)
   {     
   
         for(int i = 0; i < 8; i++)
         buffer3[i] = float_swap(buffer3[i]);    //change endian
	 
         n = sendto(sock,(char*)&buffer3, sizeof(buffer3),0,(const struct sockaddr *)&server,length);
         if (n < 0) error("Sendto");              //send data
         
         bufferHeave[3] = bufferHeave[3] - 0.00005;           //set position of heave(buffer3[3]) to -0.2
         for(int i = 0; i < 8; i++)
         buffer3[i] = bufferHeave[i];    //set buffer3 to Heave
	 

         usleep(8000);

  } 

 
   for(int i = 0; i < 8; i++)
         buffer3[i] = float_swap(buffer3[i]);    //change endian
	 

   printf("Waiting for data!\n");

   //pthread_t thread_id,thread_id_send, thread_id_recv;  //thread id
   
 //  _Bool open = 1;
   
    initscr();
    cbreak();
    noecho();           //required for curses.h
    nonl();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    
    pthread_create(&thread_id, NULL, ThreadChar, NULL);  //call thread press 'q' to exit
    pthread_create(&thread_id_recv, NULL, myThreadFun, NULL);   //call thread waiting for data
    

   while( out != 'q'){

   printw("Sending msg %lli\n", k);
   printw("Recieved msg %lli\n", k_recv);
	
       
       n = sendto(sock,(char*)&buffer3, sizeof(buffer3),0,(const struct sockaddr *)&server,length);
       if (n < 0) error("Sendto");

   
       //pthread_create(&thread_id_send, NULL, myThreadSend, NULL);   //send msg
       //pthread_join(thread_id_send, NULL);

       //pthread_create(&thread_id_recv, NULL, myThreadFun, NULL);  //call thread
       //pthread_join(thread_id_recv, NULL); 
       k++;	
       usleep(8000);  //1/60 = 0.1666 = 16.6 in milliseconds
        
      refresh();
      clear();
   }

   _Float32 buffer4[8] = {2.94e-43,0,0,0,0,0,0,0};
  
   for(int i = 0; i < 8; i++)
     buf[i] = float_swap(buffer4[i]);     //change endian
  
  endwin();

  printf("Sent %lli messages!\n", k);
  printf("Parking!\n");
  n = sendto(sock,(char*)&buf, sizeof(buf),0,(const struct sockaddr *)&server,length);  //sending data
  
  printf("Finish! \n");
  close(sock);
   
  return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}
