#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <sched.h>
#include <linux/sched.h>
#include <time.h>
#include <pthread.h>
#include <curses.h>
#include <enet/enet.h>

#define SRC_PORT_MOOG 992   //MOOG - SENDER 
#define DST_PORT_MOOG 991   //MOOG - RECEIVER
#define DST_PORT 980     //UNIX - SENDER  
#define SRC_PORT 981		//UNIX - RECEIVER 

typedef float data;

ENetAddress enetAddr;
ENetHost* enetServer;
ENetEvent  event;
ENetPacket* packet;

bool connectionEstablished = false, recievedData = false;
unsigned long long int counterSentMsg = 0, counterReceivedMsg = 0;
int socketClient, sourceSocket, ret;
unsigned int sentDataLength, receivedDataLenght;
char quit = 'c';
struct sockaddr_in server, from, srcaddr, recvaddr;
struct hostent* hp;
struct hostent* rp;

data bufferSendReceive[8] = { 1.821688e-43,0.01,0.01,-0.01,0.01,0.01,0.01,0 };
data temporaryWinsockReceiveBuffer[8] = { 1.821688e-43,0.01,0.01,-0.01,0.01,0.01,0.01,0 };

pthread_t thread_id, thread_id_send, thread_id_recv, thread_id_recv_enet, thread_id_print;  //thread id
pthread_mutex_t lock;

void error(const char*);

float swap_endianness(float value) {
	int temp = htonl(*(unsigned int*)&value);  //swaps endianness
	return *(float*)&temp;
};

void* printInfo(void* vargp)
{
	initscr();
	cbreak();
	noecho();           //required for curses.h
	nonl();
	intrflush(stdscr, FALSE);
	keypad(stdscr, TRUE);
	data printBuf[8];
	memcpy(printBuf, bufferSendReceive, sizeof(bufferSendReceive[0] * 8));
	while (quit != 'q')
	{

		printw("Sending msg %lli\n", counterSentMsg);
		printw("Recieved msg %lli\n", counterReceivedMsg);


		if (recievedData == true)
		{
			for (int i = 0; i < 8; i++)
				printBuf[i] = swap_endianness(bufferSendReceive[i]);
			recievedData = false;
		}

		for (size_t i = 0; i < 8; i++)
			printw("%.10f ", printBuf[i]);

		printw("\n");
		usleep(8000);

		refresh();
		clear();
	}
	endwin();
	pthread_exit(NULL);

}
void* threadReceiveUnix(void* vargp)
{
	pthread_detach(thread_id_recv);
	ret = recvfrom(sourceSocket, (char*)&temporaryWinsockReceiveBuffer, sizeof(temporaryWinsockReceiveBuffer), 0, (struct sockaddr*) & from, &receivedDataLenght);
	if (ret < 0) error("recvfrom");

	pthread_mutex_lock(&lock);

	memcpy(bufferSendReceive, temporaryWinsockReceiveBuffer, sizeof(temporaryWinsockReceiveBuffer[0]) * 8);
	recievedData = true;
	for (int i = 0; i < 8; i++)
		bufferSendReceive[i] = swap_endianness(bufferSendReceive[i]);

	pthread_mutex_unlock(&lock);
	counterReceivedMsg++;
	int rx_thread = pthread_create(&thread_id_recv, NULL, threadReceiveUnix, NULL);
	//if(rx_thread!=0)
		//printf("rx_thread:%d\n", rx_thread);

	pthread_exit(NULL);
}

void parkAndReleaseData()
{
	data bufferParking[8] = { 2.94e-43,0,0,0,0,0,0,0 };

	for (int i = 0; i < 8; i++)
		bufferParking[i] = swap_endianness(bufferParking[i]);     //change endian

	endwin();

	printf("Sent %lli messages!\n", counterSentMsg);
	printf("Parking!\n");
	ret = sendto(socketClient, (char*)&bufferParking, sizeof(bufferParking), 0, (const struct sockaddr*) & server, sentDataLength);  //sending data

	printf("Finish! \n");
	close(socketClient);
	enet_host_destroy(enetServer);
	enet_deinitialize();
	pthread_mutex_destroy(&lock);
	endwin();
}

void* threadReceiveEnet(void* vargp)
{
	//pthread_detach(thread_id_recv_enet);
	if (enet_initialize() != 0) {
		printf("could not initialize enet.");
		return 0;
	}

	//enet_address_set_host(&enetAddr, "127.0.0.1"/*argv[2]*/);
	// enetAddr.port = RECV_SRC_PORT;

	enetAddr.host = ENET_HOST_ANY;
	enetAddr.port = SRC_PORT;

	enetServer = enet_host_create(&enetAddr, 1, 0, 0, 0);

	if (enetServer == NULL) {
		printf("could not start server.\n");
		return 0;
	}
	else
	{
		connectionEstablished = true;
	}

	while (connectionEstablished) {
		if (enet_host_service(enetServer, &event, 0) > 0) {
			switch (event.type) {
			case ENET_EVENT_TYPE_CONNECT:
				printf("Connected!\n");
				break;

			case ENET_EVENT_TYPE_RECEIVE:
				if (&enetServer->peers[1] != event.peer) {

					recievedData = true;
					pthread_mutex_lock(&lock);
					memcpy(bufferSendReceive, (data*)event.packet->data, sizeof(data) * 8);

					for (int i = 0; i < 8; i++)
						bufferSendReceive[i] = swap_endianness(bufferSendReceive[i]);
					pthread_mutex_unlock(&lock);
					counterReceivedMsg++;
				}
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				printf("Peer has disconnected!\n");
				free(event.peer->data);
				event.peer->data = NULL;
				connectionEstablished = false;

				parkAndReleaseData();
				exit(0);

				break;

			case ENET_EVENT_TYPE_NONE:
				printf("Tick tock.\n");
				break;

			default:
				printf("Tick tock.\n");
				break;
			}

		}
	}
	//pthread_exit(NULL);
}

void* ThreadChar(void* vargp)
{
	pthread_detach(thread_id);
	while (quit != 'q') {
		quit = getch();
	}
	pthread_exit(NULL);
}

void* threadSend(void* vargp)
{

	pthread_detach(thread_id_send);
	initscr();						//initializes all curses data structures
	cbreak();						//making characters typed by the user immediately available
	noecho();                       //doesn't allow writing int terminal while running curses 
	nonl();							//curses will be able to make better use of the line-feed capability,echivalent with addch('\n')      
	intrflush(stdscr, FALSE);	    //prevents the flush
	keypad(stdscr, FALSE);			//does not treat function keys specially 
	while (quit != 'q')
	{
		pthread_mutex_lock(&lock);
		ret = sendto(socketClient, (char*)&bufferSendReceive, sizeof(bufferSendReceive), 0, (const struct sockaddr*) & server, sentDataLength);
		pthread_mutex_unlock(&lock);
		//usleep(16666);  // 1/60 = 0.01666 seconds = 16666 in microseconds
		usleep(8888);
		counterSentMsg++;
	}
	//clear();
	//refresh();
	endwin();
	pthread_exit(NULL);
}


int main(int argc, char* argv[]) {

	if(argc != 4)
	{
		printf("Please specify 3 arguments: Moog IP address and your IP address and enet or winsock.\n");
		exit(1);
	}
	
	printf("Starting program!\n");

	if (pthread_mutex_init(&lock, NULL) != 0)
	{
		printf("\n mutex init has failed\n");
		return 1;
	}

	socketClient = socket(AF_INET, SOCK_DGRAM, 0);             //CLIENT SOCKET
	if (socketClient < 0) error("socket");

	server.sin_family = AF_INET;
	hp = gethostbyname(argv[1]);                         //CLIENT IP   - kinek kuldom?
	if (hp == 0) error("Unknown host");

	bcopy((char*)hp->h_addr,
		(char*)&server.sin_addr,
		hp->h_length);


	server.sin_port = htons(DST_PORT_MOOG);    //SERVER PORT         - melyik portra?
	sentDataLength = sizeof(struct sockaddr_in);          //SERVER LENGTH

	memset(&srcaddr, 0, sizeof(srcaddr));
	srcaddr.sin_family = AF_INET;
	srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //SERVER SRCPORT
	srcaddr.sin_port = htons(SRC_PORT_MOOG);

	if (bind(socketClient, (struct sockaddr*) & srcaddr, sizeof(srcaddr)) < 0) {
		perror("bind");                                          //BIND FOR SRCPORT
		exit(1);
	}
	printf("Sending Bind ok!\n");


	if (strcmp(argv[3], "winsock") == 0)
	{
		sourceSocket = socket(AF_INET, SOCK_DGRAM, 0);         //RECV SOCKET
		if (sourceSocket < 0) error("socket");

		from.sin_family = AF_INET;
		rp = gethostbyname(argv[2]);                         //RECV IP  - az en IP-m
		if (rp == 0) error("Unknown reciever");

		bcopy((char*)rp->h_addr,
			(char*)&from.sin_addr,
			rp->h_length);
		from.sin_port = htons(DST_PORT);    //RECIVE PORT
		receivedDataLenght = sizeof(struct sockaddr_in);          //RECIVE LENGTH

		memset(&recvaddr, 0, sizeof(recvaddr));
		recvaddr.sin_family = AF_INET;
		recvaddr.sin_addr.s_addr = htonl(INADDR_ANY);     //RECIVE SRCPORT
		recvaddr.sin_port = htons(SRC_PORT);

		if (bind(sourceSocket, (struct sockaddr*) & recvaddr, sizeof(recvaddr)) < 0) {
			perror("bind");                                          //BIND FOR SRCPORT
			exit(1);
		}
		printf("Recieving Bind ok!\n");
	}

	data buffer[8] = { 1.821688e-43,0,0,0,0,0,0,0 };   //start 

	for (int i = 0; i < 8; i++)
		buffer[i] = swap_endianness(buffer[i]);      //change endian

	printf("Sending start data!\n");

	for (int i = 0; i < 5; i++) {
		ret = sendto(socketClient, (char*)&buffer, sizeof(buffer), 0, (const struct sockaddr*) & server, sentDataLength);
		if (ret < 0)                                   //sending start 5 times
			error("Sendto");

		usleep(16666.66);
	}

	data buf[8] = { 2.45e-43,0,0,0,0,0,0,0 };

	for (int i = 0; i < 8; i++)
		buf[i] = swap_endianness(buf[i]);     //change endian

	printf("Sending first data!\n");
	ret = sendto(socketClient, (char*)&buf, sizeof(buf), 0, (const struct sockaddr*) & server, sentDataLength);  //sending data


	data buf5sec[8] = { 1.821688e-43,0,0,-0.01,0,0,0,0 };

	for (int i = 0; i < 8; i++)
		buf5sec[i] = swap_endianness(buf5sec[i]);    //change endian

	printf("Wait 5 seconds\n");
	for (int i = 0; i < 600; i++)
	{

		ret = sendto(socketClient, (char*)&buf5sec, sizeof(buf5sec), 0, (const struct sockaddr*) & server, sentDataLength);
		if (ret < 0) error("Sendto error when sending data for 5 seconds");              //send data

		usleep(8000);
	}


	data bufferHeave[8] = { 1.821688e-43,0,0,-0.01,0,0,0,0 };
	printf("Setting position of heave to -0.02\n");
	printf("This will take 30 seconds\n");

	for (int i = 0; i < 3800; i++)
	{

		for (int i = 0; i < 8; i++)
			bufferSendReceive[i] = swap_endianness(bufferSendReceive[i]);    //change endian

		ret = sendto(socketClient, (char*)&bufferSendReceive, sizeof(bufferSendReceive), 0, (const struct sockaddr*) & server, sentDataLength);
		if (ret < 0) error("Sendto");              //send data

		bufferHeave[3] = bufferHeave[3] - 0.00005;           //set position of heave(buffer3[3]) to -0.2
		for (int i = 0; i < 8; i++)
			bufferSendReceive[i] = bufferHeave[i];    //set buffer3 to Heave

		usleep(8000);

	}

	for (int i = 0; i < 8; i++)
		bufferSendReceive[i] = swap_endianness(bufferSendReceive[i]);    //change endian after last assignment

	printf("Waiting for data!\n");

	initscr();		//required for exit

	pthread_create(&thread_id, NULL, ThreadChar, NULL);  //call thread

	if (strcmp(argv[3], "winsock") == 0)
	{
		pthread_create(&thread_id_recv, NULL, threadReceiveUnix, NULL);
	}
	else {
		pthread_create(&thread_id_recv_enet, NULL, threadReceiveEnet, NULL);
	}

	pthread_create(&thread_id_print, NULL, printInfo, NULL);  //call thread

	pthread_create(&thread_id_send, NULL, threadSend, NULL);   //send msg
	pthread_join(thread_id_send, NULL);

	parkAndReleaseData();
	return 0;
}

void error(const char* msg)
{
	perror(msg);
	exit(0);
}
