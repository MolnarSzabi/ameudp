#include<iostream>
#include <WS2tcpip.h>
#include <thread>

// Include the Winsock library (lib) file
#pragma comment (lib, "ws2_32.lib")        


float float_swap(float value) {
	int temp = htonl(*(unsigned int*)&value);
	return *(float*)&temp;
};

void main()
{
	// INITIALIZE WINSOCK

	// Structure to store the WinSock version. This is filled in on the call to WSAStartup()
	WSADATA data;

	// To start WinSock, the required version must be passed to
	// WSAStartup(). This server is going to use WinSock version 2 
	WORD version = MAKEWORD(2, 2);  //514

									// Start WinSock
	int wsOk = WSAStartup(version, &data);   //initiaza ws2_32.dll
	if (wsOk != 0)
	{
		std::cout << "Can't start Winsock! " << wsOk;
		return;
	}

	// SOCKET CREATION AND BINDING

	// Create a socket, notice that it is a user datagram socket (UDP)
	SOCKET in = socket(AF_INET, SOCK_DGRAM, 0);
	// Create a server hint structure for the server
	sockaddr_in serverHint;
	serverHint.sin_addr.S_un.S_addr = ADDR_ANY; // Us any IP address available on the machine
	serverHint.sin_family = AF_INET; // Address format is IPv4
	serverHint.sin_port = htons(991);//5400;//6165;//htons(54000); // Convert from little to big endian
	std::cout << "My port is: "<<991 << std::endl;
	// Try and bind the socket to the IP and port
	if (bind(in, (sockaddr*)&serverHint, sizeof(serverHint)) == SOCKET_ERROR)
	{
		std::cout << "Can't bind socket! " << WSAGetLastError() << std::endl;
		return;
		getchar();
	}

	// MAIN LOOP SETUP AND ENTRY

	sockaddr_in client; // Use to hold the client information (port / ip address)
	int clientLength = sizeof(client); // The size of the client information
	
	int counter = 0;
	float buf[8];
	//char buf[1024];
	// Enter a loop
	while (true)
	{
		ZeroMemory(&client, clientLength); // Clear the client structure
		ZeroMemory(&buf, sizeof(buf)); // Clear the receive buffer

									   // Wait for message

									   //int bytesIn = recvfrom(in, (char*)&buf, sizeof(buf), 0, (sockaddr*)&client, &clientLength);
		int bytesIn = recvfrom(in, (char*)buf, sizeof(buf), 0, (sockaddr*)&client, &clientLength);
		if (bytesIn == SOCKET_ERROR)
		{
			std::cout << "Error receiving from client " << WSAGetLastError() << std::endl;
			continue;
		}

		// Display message and client info
		char clientIp[256]; // Create enough space to convert the address byte array
		ZeroMemory(clientIp, 256); // to string of characters

		inet_ntop(AF_INET, &client.sin_addr, clientIp, 256); // Convert from byte array to chars

															 // Displ ay the message / who sent it
		
		for (size_t i = 0; i < 8; i++)
		{
			buf[i] = float_swap(buf[i]);
		}
		//for (size_t i = 0; i < 8; i++)
		//{
			//std::cout << buf[i] << " ";
			printf_s("  %.45f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n", buf[0], buf[1], buf[2] , buf[3], buf[4] , buf[5], buf[6], buf[7]);
		//}
		
		
		//std::cout << "Message recv from " << clientIp << " : " << buf[0] << std::endl;
		//printf("Message recv from %s:\n", clientIp);
		counter++;
		//if (counter >= 10000)
		//	break;
	}

	// Close socket
	closesocket(in);

	// Shutdown winsock
	WSACleanup();
}