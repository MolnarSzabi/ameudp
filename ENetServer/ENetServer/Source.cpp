//server
#include <stdio.h>
#include <enet/enet.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

typedef float data;
#define PORT  6165

ENetAddress  address;
ENetHost*    server;
ENetEvent    event;
ENetPacket*  packet;


int  main(int argc, char** argv) {
	
	int counter = 0;

	if (enet_initialize() != 0) {
		printf("Could not initialize enet.");
		return 0;
	}

	address.host = ENET_HOST_ANY;
	address.port = PORT;

	server = enet_host_create(&address, 1, 0, 0, 0);

	if (server == NULL) {
		printf("Could not start server.\n");
		return 0;
	}

	bool running = true;
	while (running) {
		if (enet_host_service(server, &event, 0) > 0) {
			//std::cout << "An event occured" << std::endl;
			switch (event.type) {

			case ENET_EVENT_TYPE_CONNECT:
				std::cout << "Connect!" << counter << std::endl;
				break;

			case ENET_EVENT_TYPE_RECEIVE:
				if (&server->peers[1] != event.peer) {
					data* a = (data*)event.packet->data;
					/*for (size_t i = 0; i < 8; i++)
						std::cout << a[i] << " ";*/

					counter++;
					//std::cout <<"Recieved count"<< counter<< std::endl;	
					printf("Recieved count: %d\n", counter);
				//	enet_host_flush(server);
				}
				
				break;

			case ENET_EVENT_TYPE_DISCONNECT:
				std::cout << "Peer has disconnected!" << std::endl;
				free(event.peer->data);
				event.peer->data = NULL;
				running = false;
				break;

			case ENET_EVENT_TYPE_NONE:
				printf("No event: Tick tock.\n");
				break;

			default:
				printf("Default: Tick tock.\n");
				break;
			}

		}
	}

	enet_host_destroy(server);
	enet_deinitialize();
}