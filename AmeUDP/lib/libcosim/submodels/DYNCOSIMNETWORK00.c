/* Submodel DYNCOSIMNETWORK00 skeleton created by AME Submodel editing utility
   ven. 11. janv. 15:30:31 2013 */



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ameutils.h"
/* *******************************************************************************
TITLE: DYNCOSIMNETWORK00
 
------------------------------------------------------------------------------
DESCRIPTION:
       Exchanges data between two AMESim models.
 
------------------------------------------------------------------------------
USAGE: Use this submodel to exchanges some data between two AMESim models or
       between an AMESim and an extern executable
 
------------------------------------------------------------------------------
PARAMETER SETTINGS:
     * ClientServer_mode : one model should act as a client, the other acts as 
      a server. The server has to be launched before the client.
     * SampleTime : data will be exchanged between client and master every
      "sampleTime"
     * server hostname : 'for slave only' name of the server
     * server port : port on which the server will listen to
 
 ------------------------------------------------------------------------------
DATE OF CREATION/AUTHOR:
       11/2006  Lionel PERRIN
 
------------------------------------------------------------------------------
INDEX OF REVISIONS:
       14/02/2011 AVL
         Added an enumeration 'initmethod' and a real parameter 'init'.
         By default (initmethod set to 1) the standard initialization method
         is used.  If initmethod set to 2, the initial output of the block is
         specified by the init real parameter. 
 
------------------------------------------------------------------------------
LIST OF FUNCTIONS USED:
 
------------------------------------------------------------------------------
******************************************************************************* */

#define _SUBMODELNAME_ "DYNCOSIMNETWORK00"

/* >>>>>>>>>>>>Insert Private Code Here. */
#include <string.h>
#include "amesocket.h"
#include "rtvalues.h"
#include "amecommunication_socket.h"

extern int ameamesock_init(amesock_conn_attributes *connection,
	int isserver, char *server_name, int port,
	int input_size, int output_size);

static amesock_conn_attributes connection_info[NUMMAX_SOCKET];
static int nb_connection_info;

/* <<<<<<<<<<<<End of Private Code. */


/* There are 2 real parameters:

   samptime   sample time    [s]
   init[]     initial output [null]
*/


/* There are 3 integer parameters:

   mode       mode                 
   serverPort server port          
   initmethod initialization method
*/


/* There is 1 text parameter:

   serverHost server hostname
*/


/* There are 2 structural parameters:

   v1 number of ports at right
   v2 number of ports at left 
*/


void dyncosimnetwork00in_(int *n, int *v1, int *v2, double *rp
      , int *ip, char **tp, double *c, void *ps[2])

{
   int loop, error;
/* >>>>>>>>>>>>Extra Initialization Function Declarations Here. */

   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;



/* <<<<<<<<<<<<End of Extra Initialization declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int init_size;
   int param_offset;  /* Offset used to assign parameter values. */
   int num_c;   /* Dynamic number of real stores. */
   int mode, serverPort, initmethod;
   double samptime, *init;
   char *serverHost;

   param_offset = 0;
   mode       = ip[0];
   serverPort = ip[1];
   initmethod = ip[2];

   param_offset = 0;
   samptime   = rp[0];
   init       = &(rp[1]);
   init_size = (*v1);
   param_offset += init_size-1;

   param_offset = 0;
   serverHost = tp[0];
   param_offset = 0;

   num_c = 2+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;

/*
   If necessary, check values of the following:

   rp[0..1]
*/


/* >>>>>>>>>>>>Initialization Function Check Statements. */
   ps[0] = (double *)malloc((2+numinputs_to_submodel) * sizeof(double));
   ps[1] = (double *)malloc((2+numoutputs_from_submodel) * sizeof(double));

   if((ps[0] == NULL) || (ps[1] == NULL))
   {
      amefprintf(stderr, "\nMemory allocation failed\n");
      error = 2;
   }

   if (*n >= NUMMAX_SOCKET)
   {
      amefprintf(stderr, "\nNumber of DYNCOSIMNETWORK instances limited to %d.\nEdit rtvalues.h to change NUMMAX_SOCKET)\n", NUMMAX_SOCKET);
      error = 2;
   }
   else if(numinputs_to_submodel > MAX_NB_VALUES_IN_AME_VECT || numoutputs_from_submodel > MAX_NB_VALUES_IN_AME_VECT)
   {
      amefprintf(stderr, "\nNumber of input/output values throw socket is limited to %d.\n", MAX_NB_VALUES_IN_AME_VECT);
      error = 2;
   }
   else
   {
      if(mode  == 2)
         amefprintf(stdout, "server is waiting for client on port %d\n", serverPort);
      else
         amefprintf(stdout, "client connected to %s:%d\n", serverHost, serverPort);
      fflush(stderr);
      if(ameamesock_init(&connection_info[*n-1], mode  == 2, serverHost, serverPort,
         2+numinputs_to_submodel, 2+numoutputs_from_submodel)!=SUCCESS)
      {
         amefprintf(stderr, "\nunable to initialize socket connection\n");
         error = 2;
      }

   }

   /* update nb_connection_info */
   if (*n > nb_connection_info)
   {
      nb_connection_info = *n;
   }

/* <<<<<<<<<<<<End of Initialization Check Statements. */

/*   Integer parameter checking:   */

   if (mode < 1 || mode > 2)
   {
      amefprintf(stderr, "\nmode must be in range [1..2].\n");
      error = 2;
   }
   if (serverPort < 1025 || serverPort > 65535)
   {
      amefprintf(stderr, "\nserver port must be in range [1025..65535].\n");
      error = 2;
   }
   if (initmethod < 1 || initmethod > 2)
   {
      amefprintf(stderr, "\ninitialization method must be in range [1..2].\n");
      error = 2;
   }

/*   Number of stores checking:   */

   if (num_c < 0)
   {
      amefprintf(stderr, "Size of real store array must be positive.\n");
      error = 2;
   }

   if(error == 1)
   {
      amefprintf(stderr, "\nWarning in %s instance %d.\n", _SUBMODELNAME_, *n);
   }
   else if(error == 2)
   {
      amefprintf(stderr, "\nFatal error in %s instance %d.\n", _SUBMODELNAME_, *n);
      amefprintf(stderr, "Terminating the program.\n");
      AmeExit(1);
   }
   param_offset += init_size-1;


/* >>>>>>>>>>>>Initialization Function Executable Statements. */
/* <<<<<<<<<<<<End of Initialization Executable Statements. */
}

void dyncosimnetwork00end_(int *n, int *v1, int *v2, double *rp
      , int *ip, char **tp, double *c, void *ps[2])

{
   int loop, error;
/* >>>>>>>>>>>>Extra Terminate Function Declarations Here. */
/* <<<<<<<<<<<<End of Extra Terminate declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int init_size;
   int param_offset;  /* Offset used to assign parameter values. */
   int num_c;   /* Dynamic number of real stores. */
   int mode, serverPort, initmethod;
   double samptime, *init;
   char *serverHost;

   param_offset = 0;
   mode       = ip[0];
   serverPort = ip[1];
   initmethod = ip[2];

   param_offset = 0;
   samptime   = rp[0];
   init       = &(rp[1]);
   init_size = (*v1);
   param_offset += init_size-1;

   param_offset = 0;
   serverHost = tp[0];
   param_offset = 0;

   num_c = 2+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;


/* >>>>>>>>>>>>Terminate Function Executable Statements. */
   if(mode  == 2)
      amefprintf(stdout, "server ended\n");
   else
      amefprintf(stdout, "client ended\n");

   amesock_close(&connection_info[*n-1]);

   free(ps[0]);
   free(ps[1]);
/* <<<<<<<<<<<<End of Terminate Executable Statements. */
}



/*  There are 2 ports.

   Port 1 has 1 variable (port_group = v1):

      1 outputs     output variable [null] basic variable output

   Port 2 has 1 variable (port_group = v2):

      1 inputs     input variable [null] basic variable input
*/

/*  There are 0 internal variables.

*/

void dyncosimnetwork00_(int *n, int *v1, int *v2, double *outputs
      , double *inputs, double *rp, int *ip, char **tp, double *c
      , void *ps[2], int *flag, double *t)

{
   int loop, logi;
/* >>>>>>>>>>>>Extra Calculation Function Declarations Here. */
   double *input_val = ps[0];
   double *output_val = ps[1];
   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;
   int index_connection_vector = *n - 1;

/* <<<<<<<<<<<<End of Extra Calculation declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int init_size;
   int param_offset;  /* Offset used to assign parameter values. */
   int num_c;   /* Dynamic number of real stores. */
   int mode, serverPort, initmethod;
   double samptime, *init;
   char *serverHost;

   param_offset = 0;
   mode       = ip[0];
   serverPort = ip[1];
   initmethod = ip[2];

   param_offset = 0;
   samptime   = rp[0];
   init       = &(rp[1]);
   init_size = (*v1);
   param_offset += init_size-1;

   param_offset = 0;
   serverHost = tp[0];
   param_offset = 0;

   num_c = 2+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   logi = 0;
   loop = 0;

/*
   Set all submodel outputs below:

   *outputs    = ??;
*/



/* >>>>>>>>>>>>Calculation Function Executable Statements. */


   /*
      c[0] is the time for the next sample
      c[1] current time
      c[2] first output
      :
      c[2+v1-1] last output
   */

   if (firstc_())
   {
      if(ConstructionLevel_() != 0)
      {
         return; /* Do not start if in consolidation step */
      }
      c[0] = *t;
   }

   if((firstc_() || *flag == 0) && *t >= c[0])
   {
      /* Time for another step. */
      c[0] += samptime;

      /* input_val and output val are made of :
      - next meeting point
      - the date of the message
      - datas.. */
      input_val[0] = c[0];
      input_val[1] = *t;
      memcpy(&input_val[2], inputs, numinputs_to_submodel*sizeof(double));
      logi = amesock_exchange(&connection_info[*n-1], input_val, output_val);
      if(logi != SUCCESS)
      {
         if(logi == CONNECTION_SHUTDOWN)
            AmeExit(0);
         else
            AmeExit(logi);
      }

      if (mode == 1) /* slave */
      {
	/* we get next meeting point from the message of the master */
         c[0] = output_val[0];
         c[1] = output_val[1];
      }

      if (firstc_() && initmethod == 2)
      /*
       * If an initial value is supplied by user,
       * output it during first call
       */
         for(loop=0; loop < numoutputs_from_submodel; loop++)
            outputs[numoutputs_from_submodel-1-loop] = init[loop];
      else
      /*
       * Create output vector from received data
       * we have to take into account that output and input
       * are not indexed with the same order
       */
         for(loop=2; loop < numoutputs_from_submodel+2; loop++ )
            outputs[numoutputs_from_submodel+1-loop]=output_val[loop];

      /* save output into c vector */
      memcpy(&c[2], outputs, numoutputs_from_submodel*sizeof(double) );
   }
   else
   {
      /* we must always set the values of a "dynamic"
      variable - the values are not kept between calls */
      memcpy(outputs, &c[2], numoutputs_from_submodel*sizeof(double) );
   }
   distim_(&c[0]);
/* <<<<<<<<<<<<End of Calculation Executable Statements. */
}

