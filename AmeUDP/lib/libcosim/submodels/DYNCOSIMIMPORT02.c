/* Submodel DYNCOSIMIMPORT02 skeleton created by AME Submodel editing utility
   Wed Dec 12 11:59:50 2018 */



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ameutils.h"
/* *******************************************************************************
TITLE :
 
------------------------------------------------------------------------------
DESCRIPTION :
 
------------------------------------------------------------------------------
USAGE :
 
------------------------------------------------------------------------------
PARAMETER SETTINGS :
 
------------------------------------------------------------------------------
REVISIONS :
 
******************************************************************************* */

#define _SUBMODELNAME_ "DYNCOSIMIMPORT02"

/* >>>>>>>>>>>>Insert Private Code Here. */
#include <string.h>
#include "import_dll_utils.h"
extern int ameload_dll(char *libname, AME_DLL *amedll);

/**
 * called at the end of the simulation: unload dll
 * \param status simulation termination status
 * \param index submodel index
 */
static void terminate_other_AMESim(int status, void *ptr)
{
   AME_DLL *amedll = ptr;
   if(amedll)
   {
      if(amedll->AMETerminate)
      {
         amedll->AMETerminate();
      }
      unloadamesimdll(amedll);
   }

}

static int myamelogfprintf(FILE *fp, const char *fmt, va_list ap)
{
#define MAXPRINTFBUFSIZE 10000
   char tmpstr[MAXPRINTFBUFSIZE];
   char tmpstr2[MAXPRINTFBUFSIZE+50];
   int ans;
   ans = vsnprintf(tmpstr, MAXPRINTFBUFSIZE, fmt, ap);
   tmpstr[MAXPRINTFBUFSIZE]='\0';
   strcpy(tmpstr2,"DYNCOSIMIMPORT02> :DLL: ");
   if(tmpstr[0]=='\n')
   {
      tmpstr[0]=' ';
   }
   strcat(tmpstr2,tmpstr);
   amemessage(tmpstr2);
   return ans;
}

typedef struct {
   AME_DLL* amedll;
   unsigned int *order;
   AMECOSIM_MASTER_PTR masterCS;
} dynImportData;

static void delete_dynImportData(dynImportData* pData)
{
   if(pData) {
      free(pData->amedll);
      free(pData->order);
      deleteCosimMaster(&pData->masterCS);
   }
}

static dynImportData* create_dynImportData(unsigned int nbinput, unsigned int order, unsigned int extraType)
{
   int res = 0;

   dynImportData *pData = (dynImportData*)calloc(1, sizeof(dynImportData));

   if(pData) {
      pData->amedll = (AME_DLL*)calloc(1, sizeof(AME_DLL));
      if(nbinput > 0) {
         pData->order = (unsigned int*)calloc(nbinput, sizeof(unsigned int));

         if(pData->order) {
            unsigned int i;
            for(i = 0; i< nbinput; i++) {
               pData->order[i] = order;
            }
         }
         else {
            res = 1;
         }

      }
      if(createCosimMaster(&pData->masterCS, nbinput, extraType-1) != AME_NO_ERROR) {
         res = 1;
      }
   }

   if(res) {
      delete_dynImportData(pData);
      pData = NULL;
   }

   return pData;
}

static int readcheck_dll(dynImportData *pData, char* dllName, int usefixedstepsolver, double samptime)
{
   int error = 0;

   if (ameload_dll(dllName, pData->amedll) == 0)
   {
      amefprintf(stderr,"DYNCOSIMIMPORT02> Failed to load \"%s\"\n", dllName);
      error = 2;
   }
   else
   {
      /* We need also to verify that the DLL we read is really an AMESim DLL.  If the load_dll
         function have found and set the following pointers we should be OK.

         amedll->AMEInitModel refers to  "AMEInitModel"
         amedll->AMEdoAStep2  refers to "AMEdoAStep2"
         amedll->AMETerminate refers to "AMETerminate"
         amedll->AMEWorkingDir  refers to  "AMEWorkingDir"
      */
      if(pData->amedll->AMEInitModel==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEInitModel not found\n");
         error = 2;
      }
      if(pData->amedll->AMEdoAStep2==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEdoAStep not found\n");
         error = 2;
      }
      if(pData->amedll->AMETerminate==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMETerminate not found\n");
         error = 2;
      }
      if(pData->amedll->AMESetFinalTime== NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMESetFinalTime not found\n");
         error = 2;
      }
      if(pData->amedll->AMEWorkingDir == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEWorkingDir not found\n");
         error = 2;
      }

      /* We will accept older amesim dlls that might be missing the following */
      if(pData->amedll->AMEInitModelFailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEInitModelFailSafe not found\n");
         error = 1;
      }
      if(pData->amedll->AMEdoAStep2FailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEdoAStep2FailSafe not found\n");
         error = 1;
      }
      if(pData->amedll->AMEdoAStepFailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEdoAStepFailSafe not found\n");
         error = 1;
      }

      if (usefixedstepsolver)
      {
         if((pData->amedll->AMESetUpFixedStepSolver2 == NULL) && (pData->amedll->AMESetUpFixedStepSolver == NULL))
         {
            amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMESetUpFixedStepSolver not found\n");
            error = 2;
         }
         if(pData->amedll->AMESetUpFixedStepSolverFailSafe2 == NULL)
         {
            amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMESetUpFixedStepSolverFailSafe2 not found\n");
            error = 1;
         }
      }

      if(pData->amedll->AMEdoExtrapolatedStep == NULL) {
         amefprintf(stderr,"DYNCOSIMIMPORT02> Entrypoint AMEdoExtrapolatedStep not found.");
         amefprintf(stderr,"\nTo be able to use extrapolation co-simulation submodel, please compile the slave model. Co-simulation aborted.\n");
         error = 2;
      }
   }

   if(isfixedstepsolver_()) {
      if(getfixedtimestep_() > samptime) {
         amefprintf(stderr, "\nStep of the fixed step solver must be less than or equal to the sample time. Co-simulation aborted.\n");
         error = 2;
      }
   }

   return error;
}

/* <<<<<<<<<<<<End of Private Code. */


/* There are 3 real parameters:

   samptime   sample time          [s]
   printinter slave print interval [s]
   tol        slave tolerance      [null]
*/


/* There are 9 integer parameters:

   errtype            error type             
   writelevel         write level            
   discprintouts      discontinuity printouts
   runstats           runstats               
   usefixedstepsolver slave solver type      
   oversample         slave oversample       
   RKorder            slave method order     
   extraOrder         extrapolation order    
   extraType          extrapolation type     
*/


/* There is 1 text parameter:

   dllName slave dynamic library
*/


/* There are 2 structural parameters:

   v1 number of ports at right
   v2 number of ports at left 
*/


void dyncosimimport02in_(int *n, int *v1, int *v2, double *rp, int *ip
      , char **tp, double c[1], void *ps[1], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Initialization Function Declarations Here. */
   char tmpfullname[1024];
   char *dirsep;
   dynImportData *pData;
/* <<<<<<<<<<<<End of Extra Initialization declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, extraOrder, extraType;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   extraOrder = ip[7];
   extraType  = ip[8];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;

/*
   If necessary, check values of the following:

   rp[0..2]
*/

/*
   Check and/or reset the following fixed and/or discrete variable

   *outputs    = ??;
*/


/* >>>>>>>>>>>>Initialization Function Check Statements. */

   pData = create_dynImportData((unsigned int)inputs_size, (unsigned int)extraOrder, (unsigned int)extraType);

   if(!pData) {
      amefprintf(stderr,"DYNCOSIMIMPORT02> Cannot instanciate internal ressources.\n");
      error = 2;
   }

   ps[0] = pData;

   if(error != 2) {
      error = readcheck_dll(pData, dllName, usefixedstepsolver, samptime);
   }

   if (error != 2)
   {
      /* If the DLL is in a diffrent directory we need to call the "AMEWorkingDir" function
         so that the DLL reads the data files from that directory and it writes the result file
         in that directory. We only need to do this when the name of the DLL contains a "/" */
      strcpy(tmpfullname,tp[0]);
      dirsep = strrchr(tmpfullname,'/');
      if(dirsep)
      {
         dirsep++;
         *dirsep = '\0';
         pData->amedll->AMEWorkingDir(tmpfullname);
      }

      /* set slave final time */
      pData->amedll->AMESetFinalTime(getfinaltime_());

      pData->amedll->AMEInstallfprintf((void*)myamelogfprintf);
   }

/* <<<<<<<<<<<<End of Initialization Check Statements. */

/*   Integer parameter checking:   */

   if (errtype < 1 || errtype > 3)
   {
      amefprintf(stderr, "\nerror type must be in range [1..3].\n");
      error = 2;
   }
   if (writelevel < 1 || writelevel > 3)
   {
      amefprintf(stderr, "\nwrite level must be in range [1..3].\n");
      error = 2;
   }
   if (discprintouts < 1 || discprintouts > 2)
   {
      amefprintf(stderr, "\ndiscontinuity printouts must be in range [1..2].\n");
      error = 2;
   }
   if (runstats < 1 || runstats > 2)
   {
      amefprintf(stderr, "\nrunstats must be in range [1..2].\n");
      error = 2;
   }
   if (usefixedstepsolver < 1 || usefixedstepsolver > 2)
   {
      amefprintf(stderr, "\nslave solver type must be in range [1..2].\n");
      error = 2;
   }
   if (oversample < 1 || oversample > 1000000)
   {
      amefprintf(stderr, "\nslave oversample must be in range [1..1000000].\n");
      error = 2;
   }
   if (RKorder < 1 || RKorder > 4)
   {
      amefprintf(stderr, "\nslave method order must be in range [1..4].\n");
      error = 2;
   }
   if (extraOrder < 0 || extraOrder > 2)
   {
      amefprintf(stderr, "\nextrapolation order must be in range [0..2].\n");
      error = 2;
   }
   if (extraType < 1 || extraType > 2)
   {
      amefprintf(stderr, "\nextrapolation type must be in range [1..2].\n");
      error = 2;
   }

   if(error == 1)
   {
      amefprintf(stderr, "\nWarning in %s instance %d.\n", _SUBMODELNAME_, *n);
   }
   else if(error == 2)
   {
      amefprintf(stderr, "\nFatal error in %s instance %d.\n", _SUBMODELNAME_, *n);
      amefprintf(stderr, "Terminating the program.\n");
      AmeExit(1);
   }


/* >>>>>>>>>>>>Initialization Function Executable Statements. */
/* <<<<<<<<<<<<End of Initialization Executable Statements. */
}

void dyncosimimport02end_(int *n, int *v1, int *v2, double *rp
      , int *ip, char **tp, double c[1], void *ps[1], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Terminate Function Declarations Here. */
   dynImportData *pData = ps[0];
/* <<<<<<<<<<<<End of Extra Terminate declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, extraOrder, extraType;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   extraOrder = ip[7];
   extraType  = ip[8];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;


/* >>>>>>>>>>>>Terminate Function Executable Statements. */
   if(pData) {
      terminate_other_AMESim(0, pData->amedll);
      delete_dynImportData(pData);
   }
/* <<<<<<<<<<<<End of Terminate Executable Statements. */
}



/*  There are 2 ports.

   Port 1 has 1 variable (port_group = v1):

      1 outputs     output variable [null] discrete

   Port 2 has 1 variable (port_group = v2):

      1 inputs     input variable [null] basic variable input
*/

/*  There are 0 internal variables.

*/

void dyncosimimport02_(int *n, int *v1, int *v2, double *outputs
      , double *inputs, double *rp, int *ip, char **tp, double c[1]
      , void *ps[1], int *flag, double *t)

{
   int loop, logi;
/* >>>>>>>>>>>>Extra Calculation Function Declarations Here. */
   int run_type = 8;
   int solver_type=0;
   dynImportData *pData = ps[0];
   int isFirstCall;
   int retval = 0;
/* <<<<<<<<<<<<End of Extra Calculation declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, extraOrder, extraType;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   extraOrder = ip[7];
   extraType  = ip[8];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   logi = 0;
   loop = 0;

/*
   Set all submodel outputs below:

*/


/*
   The following discrete variable(s) can be reset when the discontinuity flag is zero:

   *outputs    = ??;
*/



/* >>>>>>>>>>>>Calculation Function Executable Statements. */
   errtype--;
   writelevel--;
   discprintouts--;
   runstats--;
   usefixedstepsolver--;

   isFirstCall = firstc_();

   /*
      c[0] is the time for the next sample
      c[1] current time
   */

   if(isFirstCall)
   {
      c[0] = *t;
   }

   if(isFirstCall || (*flag == 0))
   {
      int doastep=0;
      if(isFirstCall)
      {
         if(ConstructionLevel_() != 0)
         {
            return; /* Do not start if in consolidation step */
         }
			/* Initialize model */
         if (!usefixedstepsolver)
         {
            if(0 != pData->amedll->initvarstep(pData->amedll,
                                        *t, printinter, samptime,
                                        tol,  errtype, writelevel, discprintouts, runstats, run_type, 0,
                                        inputs_size, outputs_size, inputs, outputs))
            {
               amewarning("%s instance %d > Couldn't initialize model '%s' for variable step solver.\n",_SUBMODELNAME_, *n, dllName);
               AmeExit(1);
            }
         }
         else
         {
            if(0 != pData->amedll->initfixedstep(pData->amedll, *t, inputs_size, outputs_size, run_type,
                                          solver_type, RKorder, samptime/oversample, printinter))
            {
               amewarning("%s instance %d > Couldn't initialize model '%s' for fixed step solver.\n",_SUBMODELNAME_, *n, dllName);
               AmeExit(1);
            }
         }
         if(inputs_size > 0) {
            retval = updateHistory(pData->masterCS, *t, inputs);
         }
      }

      if(!isfixedstepsolver_())
      {
         if(*t >= c[0])
         {
            doastep = 1;
         }
      }
      else
      {
         if ( (*t >= c[0]) && !isintermstep_())
         {
            doastep = 1;
         }
      }

      if(doastep)
      {
         /* Time for another step. */
         if(inputs_size > 0) {
            double **poly2transfert;

            if(!isFirstCall && (extraType == 2)) {
               retval |= updateHistory(pData->masterCS, *t, inputs);
            }

            retval |= updatePolynomial(pData->masterCS, pData->order, &poly2transfert);

            /* Now request the step */
            retval = pData->amedll->AMEdoExtrapolatedStep(*t, pData->order, (const double**)poly2transfert, outputs);

            if(!isFirstCall && (extraType == 1)) {
               retval |= updateHistory(pData->masterCS, *t, inputs);
            }
         }
         else {
            /* Now request the step */
            retval = pData->amedll->AMEdoExtrapolatedStep(*t, NULL, NULL, outputs);
         }

         if(retval != 0)
         {
            amewarning("%s instance %d > Couldn't run model '%s'.\n",_SUBMODELNAME_, *n, dllName);
            AmeExit(1);
         }
         c[0] = c[0] + samptime;
      }
   }

   distim_(&c[0]);

/* <<<<<<<<<<<<End of Calculation Executable Statements. */
}

