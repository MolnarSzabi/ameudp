/* Submodel DYNCOSIMIMPORT01 skeleton created by AME Submodel editing utility
   Mon Oct 1 10:56:13 2018 */



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ameutils.h"
/* *******************************************************************************
TITLE :
 
------------------------------------------------------------------------------
DESCRIPTION :
 
------------------------------------------------------------------------------
USAGE :
 
------------------------------------------------------------------------------
PARAMETER SETTINGS :
 
------------------------------------------------------------------------------
REVISIONS :
 
******************************************************************************* */

#define _SUBMODELNAME_ "DYNCOSIMIMPORT01"

/* >>>>>>>>>>>>Insert Private Code Here. */
#include <string.h>
#include "import_dll_utils.h"
extern int ameload_dll(char *libname, AME_DLL *amedll);

/**
 * called at the end of the simulation: unload dll
 * \param status simulation termination status
 * \param index submodel index
 */
static void terminate_other_AMESim(int status, void *ptr)
{
   AME_DLL *amedll = ptr;
   if(amedll)
   {
      if(amedll->AMETerminate)
      {
         amedll->AMETerminate();
      }
      unloadamesimdll(amedll);
   }

}

static int myamelogfprintf(FILE *fp, const char *fmt, va_list ap)
{
#define MAXPRINTFBUFSIZE 10000
   char tmpstr[MAXPRINTFBUFSIZE];
   char tmpstr2[MAXPRINTFBUFSIZE+50];
   int ans;
   ans = vsnprintf(tmpstr, MAXPRINTFBUFSIZE, fmt, ap);
   tmpstr[MAXPRINTFBUFSIZE]='\0';
   strcpy(tmpstr2,"DYNCOSIMIMPORT01> :DLL: ");
   if(tmpstr[0]=='\n')
   {
      tmpstr[0]=' ';
   }
   strcat(tmpstr2,tmpstr);
   amemessage(tmpstr2);
   return ans;
}
/* <<<<<<<<<<<<End of Private Code. */


/* There are 3 real parameters:

   samptime   sample time          [s]
   printinter slave print interval [s]
   tol        slave tolerance      [null]
*/


/* There are 8 integer parameters:

   errtype            error type             
   writelevel         write level            
   discprintouts      discontinuity printouts
   runstats           runstats               
   usefixedstepsolver slave solver type      
   oversample         slave oversample       
   RKorder            slave method order     
   smthdisc           Smooth discontinuity   
*/


/* There is 1 text parameter:

   dllName slave dynamic library
*/


/* There are 2 structural parameters:

   v1 number of ports at right
   v2 number of ports at left 
*/


void dyncosimimport01in_(int *n, int *v1, int *v2, double *rp, int *ip
      , char **tp, double c[1], void *ps[2], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Initialization Function Declarations Here. */
   char tmpfullname[1024];
   char *dirsep;
   AME_DLL* amedll=NULL;
/* <<<<<<<<<<<<End of Extra Initialization declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, smthdisc;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   smthdisc   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;

/*
   If necessary, check values of the following:

   rp[0..2]
*/

/*
   Check and/or reset the following fixed and/or discrete variable

   *outputs    = ??;
*/


/* >>>>>>>>>>>>Initialization Function Check Statements. */

   amedll = calloc(1, sizeof(AME_DLL));
   if (ameload_dll(tp[0], amedll) == 0)
   {
      amefprintf(stderr,"DYNCOSIMIMPORT01> Failed to load \"%s\"\n", tp[0]);
      error = 2;
   }
   else
   {
      /* We need also to verify that the DLL we read is really an AMESim DLL.  If the load_dll
         function have found and set the following pointers we should be OK.

         amedll->AMEInitModel refers to  "AMEInitModel"
         amedll->AMEdoAStep2  refers to "AMEdoAStep2"
         amedll->AMETerminate refers to "AMETerminate"
         amedll->AMEWorkingDir  refers to  "AMEWorkingDir"
      */
      if(amedll->AMEInitModel==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEInitModel not found\n");
         error = 2;
      }
      if(amedll->AMEdoAStep2==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEdoAStep not found\n");
         error = 2;
      }
      if(amedll->AMETerminate==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMETerminate not found\n");
         error = 2;
      }
      if(amedll->AMESetFinalTime== NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMESetFinalTime not found\n");
         error = 2;
      }
      if(amedll->AMEWorkingDir == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEWorkingDir not found\n");
         error = 2;
      }

      /* We will accept older amesim dlls that might be missing the following */
      if(amedll->AMEInitModelFailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEInitModelFailSafe not found\n");
         error = 1;
      }
      if(amedll->AMEdoAStep2FailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEdoAStep2FailSafe not found\n");
         error = 1;
      }
      if(amedll->AMEdoAStepFailSafe == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMEdoAStepFailSafe not found\n");
         error = 1;
      }


      if (usefixedstepsolver)
      {
         if((amedll->AMESetUpFixedStepSolver2 == NULL) && (amedll->AMESetUpFixedStepSolver == NULL))
         {
            amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMESetUpFixedStepSolver not found\n");
            error = 2;
         }
         if(amedll->AMESetUpFixedStepSolverFailSafe2 == NULL)
         {
            amefprintf(stderr,"DYNCOSIMIMPORT01> Entrypoint AMESetUpFixedStepSolverFailSafe2 not found\n");
            error = 1;
         }
      }
   }

   if(isfixedstepsolver_()) {
      if(getfixedtimestep_() > samptime) {
         amefprintf(stderr, "\nStep of the fixed step solver must be less than or equal to the sample time. Co-simulation aborted.\n");
         error = 2;
      }
   }

   if (error != 2)
   {
      /* If the DLL is in a diffrent directory we need to call the "AMEWorkingDir" function
         so that the DLL reads the data files from that directory and it writes the result file
         in that directory. We only need to do this when the name of the DLL contains a "/" */
      strcpy(tmpfullname,tp[0]);
      dirsep = strrchr(tmpfullname,'/');
      if(dirsep)
      {
         dirsep++;
         *dirsep = '\0';
         amedll->AMEWorkingDir(tmpfullname);
      }

      /* set slave final time */
      amedll->AMESetFinalTime(getfinaltime_());

      amedll->AMEInstallfprintf((void*)myamelogfprintf);
   }

   ps[0] = amedll; /* transfer to pointer store */

   if(inputs_size > 0) {
      ps[1] = (double*)calloc((size_t)inputs_size, sizeof(double));
   }
   else {
      ps[1] = NULL;
   }

/* <<<<<<<<<<<<End of Initialization Check Statements. */

/*   Integer parameter checking:   */

   if (errtype < 1 || errtype > 3)
   {
      amefprintf(stderr, "\nerror type must be in range [1..3].\n");
      error = 2;
   }
   if (writelevel < 1 || writelevel > 3)
   {
      amefprintf(stderr, "\nwrite level must be in range [1..3].\n");
      error = 2;
   }
   if (discprintouts < 1 || discprintouts > 2)
   {
      amefprintf(stderr, "\ndiscontinuity printouts must be in range [1..2].\n");
      error = 2;
   }
   if (runstats < 1 || runstats > 2)
   {
      amefprintf(stderr, "\nrunstats must be in range [1..2].\n");
      error = 2;
   }
   if (usefixedstepsolver < 1 || usefixedstepsolver > 2)
   {
      amefprintf(stderr, "\nslave solver type must be in range [1..2].\n");
      error = 2;
   }
   if (oversample < 1 || oversample > 1000000)
   {
      amefprintf(stderr, "\nslave oversample must be in range [1..1000000].\n");
      error = 2;
   }
   if (RKorder < 1 || RKorder > 4)
   {
      amefprintf(stderr, "\nslave method order must be in range [1..4].\n");
      error = 2;
   }
   if (smthdisc < 1 || smthdisc > 2)
   {
      amefprintf(stderr, "\nSmooth discontinuity must be in range [1..2].\n");
      error = 2;
   }

   if(error == 1)
   {
      amefprintf(stderr, "\nWarning in %s instance %d.\n", _SUBMODELNAME_, *n);
   }
   else if(error == 2)
   {
      amefprintf(stderr, "\nFatal error in %s instance %d.\n", _SUBMODELNAME_, *n);
      amefprintf(stderr, "Terminating the program.\n");
      AmeExit(1);
   }


/* >>>>>>>>>>>>Initialization Function Executable Statements. */
/* <<<<<<<<<<<<End of Initialization Executable Statements. */
}

void dyncosimimport01end_(int *n, int *v1, int *v2, double *rp
      , int *ip, char **tp, double c[1], void *ps[2], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Terminate Function Declarations Here. */
/* <<<<<<<<<<<<End of Extra Terminate declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, smthdisc;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   smthdisc   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;


/* >>>>>>>>>>>>Terminate Function Executable Statements. */
   terminate_other_AMESim(0, ps[0]);
   free(ps[1]);
/* <<<<<<<<<<<<End of Terminate Executable Statements. */
}



/*  There are 2 ports.

   Port 1 has 1 variable (port_group = v1):

      1 outputs     output variable [null] discrete

   Port 2 has 1 variable (port_group = v2):

      1 inputs     input variable [null] basic variable input
*/

/*  There are 0 internal variables.

*/

void dyncosimimport01_(int *n, int *v1, int *v2, double *outputs
      , double *inputs, double *rp, int *ip, char **tp, double c[1]
      , void *ps[2], int *flag, double *t)

{
   int loop, logi;
/* >>>>>>>>>>>>Extra Calculation Function Declarations Here. */
   int run_type = 8;
   int solver_type=0;
   AME_DLL *amedll=ps[0];
/* <<<<<<<<<<<<End of Extra Calculation declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, smthdisc;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   smthdisc   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   logi = 0;
   loop = 0;

/*
   Set all submodel outputs below:

*/


/*
   The following discrete variable(s) can be reset when the discontinuity flag is zero:

   *outputs    = ??;
*/



/* >>>>>>>>>>>>Calculation Function Executable Statements. */
   errtype--;
   writelevel--;
   discprintouts--;
   runstats--;
   usefixedstepsolver--;

   /*
      c[0] is the time for the next sample
      c[1] current time
   */

   if (firstc_())
   {
      c[0] = *t;
   }

   if(firstc_() || (*flag == 0))
   {
      int doastep=0;
      if (firstc_())
      {
         if(ConstructionLevel_() != 0)
         {
            return; /* Do not start if in consolidation step */
         }
			/* Initialize model */
         if (!usefixedstepsolver)
         {
            if(0 != amedll->initvarstep(amedll,
                                        *t, printinter, samptime,
                                        tol,  errtype, writelevel, discprintouts, runstats, run_type, 0,
                                        inputs_size, outputs_size, inputs, outputs))
            {
               amewarning("%s instance %d > Couldn't initialize model '%s' for variable step solver.\n",_SUBMODELNAME_, *n, dllName);
               AmeExit(1);
            }
         }
         else
         {
            if(0 != amedll->initfixedstep(amedll, *t, inputs_size, outputs_size, run_type,
                                          solver_type, RKorder, samptime/oversample, printinter))
            {
               amewarning("%s instance %d > Couldn't initialize model '%s' for fixed step solver.\n",_SUBMODELNAME_, *n, dllName);
               AmeExit(1);
            }
         }
         if(inputs_size > 0) {
            memcpy(ps[1], inputs, inputs_size*sizeof(double));
         }
      }

      if(!isfixedstepsolver_())
      {
         if(*t >= c[0])
         {
            doastep = 1;
         }
      }
      else
      {
         if ( (*t >= c[0]) && !isintermstep_())
         {
            doastep = 1;
         }
      }

      if(doastep)
      {
         int retval;
         /* Time for another step. */
         if(smthdisc == 2) {
            retval = amedll->dostep(amedll, *t, inputs_size, outputs_size, inputs, outputs);
         }
         else {
            retval = amedll->dostep2(amedll, *t, inputs_size, outputs_size, ps[1], outputs);
            if(inputs_size > 0) {
               memcpy(ps[1], inputs, inputs_size*sizeof(double));
            }
         }

         if(retval != 0)
         {
            amewarning("%s instance %d > Couldn't run model '%s'.\n",_SUBMODELNAME_, *n, dllName);
            AmeExit(1);
         }
         c[0] = c[0] + samptime;
      }
   }

   distim_(&c[0]);

/* <<<<<<<<<<<<End of Calculation Executable Statements. */
}

