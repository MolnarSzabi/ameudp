/* Submodel DYNCOSIMSHM01 skeleton created by AME Submodel editing utility
   Fri Oct 6 17:33:17 2017 */



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ameutils.h"
/* *******************************************************************************
TITLE : DYNCOSIMSHM01
 
------------------------------------------------------------------------------
DESCRIPTION:
       Exchanges data between two AMESim models.
 
------------------------------------------------------------------------------
USAGE: Use this submodel to exchanges some data between two AMESim models or
       between an AMESim and an extern executable.
 
------------------------------------------------------------------------------
PARAMETER SETTINGS:
    * ClientServer_mode : one model should act as a client, the other acts as 
      a server. The server must be launch at first and will listen for a client
      connection.
    * SampleTime : data will be exchanged between client and server every
      "sampleTime"
    * server name : 'for client only' name of the machine on which server runs.
    * server port : tcp port on which the server is listening to.
   
 ------------------------------------------------------------------------------
DATE OF CREATION/AUTHOR:
       11/2006  Lionel PERRIN
 
------------------------------------------------------------------------------
INDEX OF REVISIONS:
       02/02/2011 AVL
         Added an enumeration 'initmethod' and a real parameter 'init'.
         By default (initmethod set to 1) the standard initialization method
         is used.  If initmethod set to 2, the initial output of the block is
         specified by the init real parameter. 
 
------------------------------------------------------------------------------
******************************************************************************* */

#define _SUBMODELNAME_ "DYNCOSIMSHM01"

/* >>>>>>>>>>>>Insert Private Code Here. */
#include <string.h>
#include "rtvalues.h"
#include "amecommunication_shm.h"
extern int ameameshm_init(ameshm_conn_attributes *connection,
 int ismaster, char *name, int input_size, int output_size);
static ameshm_conn_attributes connection_info[NUMMAX_SOCKET];
static int nb_connection_info;

/* <<<<<<<<<<<<End of Private Code. */


/* There is 1 real parameter:

   samptime sample time [s]
*/


/* There is 1 integer parameter:

   mode mode
*/


/* There is 1 text parameter:

   shmName shared memory identifier
*/


/* There are 2 structural parameters:

   v1 number of ports at right
   v2 number of ports at left 
*/


void dyncosimshm01in_(int *n, int *v1, int *v2, double *rp, int *ip
      , char **tp, double c[2], void *ps[2], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Initialization Function Declarations Here. */

   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;

/* <<<<<<<<<<<<End of Extra Initialization declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int mode;
   double samptime;
   char *shmName;

   mode       = ip[0];

   samptime   = rp[0];

   shmName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;

/*
   If necessary, check values of the following:

   rp[0..0]
*/

/*
   Check and/or reset the following fixed and/or discrete variable

   *outputs    = ??;
*/


/* >>>>>>>>>>>>Initialization Function Check Statements. */
   ps[0] = (double *)malloc((2+numinputs_to_submodel) * sizeof(double));
   ps[1] = (double *)malloc((2+numoutputs_from_submodel) * sizeof(double));

   if((ps[0] == NULL) || (ps[1] == NULL))
   {
      amefprintf(stderr, "\nMemory allocation failed\n");
      error = 2;
   }

   if(mode == 2) {
      /* Master : check if sample time > fixed step */
      if(isfixedstepsolver_()) {
         if(getfixedtimestep_() > samptime) {
            amefprintf(stderr, "\nStep of the fixed step solver must be less than or equal to the sample time. Co-simulation aborted.\n");
            error = 2;
         }
      }
   }

   if (*n >= NUMMAX_SOCKET)
   {
      amefprintf(stderr, "\nNumber of DYNCOSIMSHM instances limited to %d.\nEdit rtvalues.h to change NUMMAX_SOCKET)\n", NUMMAX_SOCKET);
      error = 2;
   }

   if(error == 0)
   {
      if(mode  == 2)
         amefprintf(stdout, "master launched\n");
      else
         amefprintf(stdout, "slave launched\n");
      fflush(stderr);
      if(ameameshm_init(&connection_info[*n-1], mode  == 2, tp[0], 2+numinputs_to_submodel, 2+numoutputs_from_submodel)!=SUCCESS)
      {
         amefprintf(stderr, "\nunable to initialize sharedmemory\n");
         error = 2;
      }

   }

   /* update nb_connection_info */
   if (*n > nb_connection_info)
   {
      nb_connection_info = *n;
   }

/* <<<<<<<<<<<<End of Initialization Check Statements. */

/*   Integer parameter checking:   */

   if (mode < 1 || mode > 2)
   {
      amefprintf(stderr, "\nmode must be in range [1..2].\n");
      error = 2;
   }

   if(error == 1)
   {
      amefprintf(stderr, "\nWarning in %s instance %d.\n", _SUBMODELNAME_, *n);
   }
   else if(error == 2)
   {
      amefprintf(stderr, "\nFatal error in %s instance %d.\n", _SUBMODELNAME_, *n);
      amefprintf(stderr, "Terminating the program.\n");
      AmeExit(1);
   }


/* >>>>>>>>>>>>Initialization Function Executable Statements. */

/* <<<<<<<<<<<<End of Initialization Executable Statements. */
}

void dyncosimshm01end_(int *n, int *v1, int *v2, double *rp, int *ip
      , char **tp, double c[2], void *ps[2], double *outputs)

{
   int loop, error;
/* >>>>>>>>>>>>Extra Terminate Function Declarations Here. */
/* <<<<<<<<<<<<End of Extra Terminate declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int mode;
   double samptime;
   char *shmName;

   mode       = ip[0];

   samptime   = rp[0];

   shmName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;


/* >>>>>>>>>>>>Terminate Function Executable Statements. */
   if(mode  == 2)
      amefprintf(stdout, "master ended\n");
   else
      amefprintf(stdout, "slave ended\n");

   if(connection_info[*n-1].name)
   {
      ameshm_close(&connection_info[*n-1]);
   }

   if(ps[0])
   {
      free(ps[0]);
      ps[0] = NULL;
   }

   if(ps[1])
   {
      free(ps[1]);
      ps[1] = NULL;
   }

/* <<<<<<<<<<<<End of Terminate Executable Statements. */
}



/*  There are 2 ports.

   Port 1 has 1 variable (port_group = v1):

      1 outputs     output variable [null] discrete

   Port 2 has 1 variable (port_group = v2):

      1 inputs     input variable [null] basic variable input
*/

/*  There are 0 internal variables.

*/

void dyncosimshm01_(int *n, int *v1, int *v2, double *outputs
      , double *inputs, double *rp, int *ip, char **tp, double c[2]
      , void *ps[2], int *flag, double *t)

{
   int loop, logi;
/* >>>>>>>>>>>>Extra Calculation Function Declarations Here. */
   double *input_val = ps[0];
   double *output_val = ps[1];
   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;
   int index_connection_vector = *n - 1;

/* <<<<<<<<<<<<End of Extra Calculation declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int mode;
   double samptime;
   char *shmName;

   mode       = ip[0];

   samptime   = rp[0];

   shmName    = tp[0];
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   logi = 0;
   loop = 0;

/*
   Set all submodel outputs below:

*/


/*
   The following discrete variable(s) can be reset when the discontinuity flag is zero:

   *outputs    = ??;
*/



/* >>>>>>>>>>>>Calculation Function Executable Statements. */


   /*
      c[0] is the time for the next sample
      c[1] current time
   */

   if (firstc_())
   {
      if(ConstructionLevel_() != 0)
      {
         return; /* Do not start if in consolidation step */
      }
      c[0] = *t;
   }

   if((firstc_() || *flag == 0) && *t >= c[0])
   {
      /* Time for another step. */
      c[0] += samptime;

      /* input_val and output val are made of :
      - next meeting point
      - the date of the message
      - datas.. */
      input_val[0] = c[0];
      input_val[1] = *t;
      memcpy(&input_val[2], inputs, numinputs_to_submodel*sizeof(double));
      logi = ameshm_exchange(&connection_info[*n-1], input_val, output_val);
      if(logi != SUCCESS)
      {
         if(logi == CONNECTION_SHUTDOWN)
            AmeExit(0);
         else
            AmeExit(logi);
      }

      if (mode == 1) /* slave */
      {
         if(firstc_()) {
            /* Slave : check if sample time > fixed step */
            if( isfixedstepsolver_() ) {
               if( getfixedtimestep_() > (output_val[0]-*t) ) {
                  amefprintf(stderr, "\nStep of the fixed step solver must be less than or equal to the sample time. Co-simulation aborted.\n");
                  AmeExit(1);
               }
            }
         }
         /* we get next meeting point from the message of the master */
         c[0] = output_val[0];
         c[1] = output_val[1];
      }

      /*
       * Create output vector from received data.
       * we have to take into account that output and input
       * are not indexed with the same order
       */
      for(loop=2; loop < numoutputs_from_submodel+2; loop++ )
         outputs[numoutputs_from_submodel+1-loop]=output_val[loop];
   }

   distim_(&c[0]);
/* <<<<<<<<<<<<End of Calculation Executable Statements. */
}

