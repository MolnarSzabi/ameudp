/* Submodel DYNCOSIMIMPORT00 skeleton created by AME Submodel editing utility
   ven. 11. janv. 17:46:15 2013 */



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "ameutils.h"
/* *******************************************************************************
TITLE :
 
------------------------------------------------------------------------------
DESCRIPTION :
 
------------------------------------------------------------------------------
USAGE :
 
------------------------------------------------------------------------------
PARAMETER SETTINGS :
 
------------------------------------------------------------------------------
REVISIONS :
 
******************************************************************************* */

#define _SUBMODELNAME_ "DYNCOSIMIMPORT00"

/* >>>>>>>>>>>>Insert Private Code Here. */
#include <string.h>
#include "import_dll_utils.h"
extern int ameload_dll(char *libname, AME_DLL *amedll);

/* <<<<<<<<<<<<End of Private Code. */


/* There are 3 real parameters:

   samptime   sample time          [s]
   printinter slave print interval [s]
   tol        slave tolerance      [null]
*/


/* There are 8 integer parameters:

   errtype            error type             
   writelevel         write level            
   discprintouts      discontinuity printouts
   runstats           runstats               
   usefixedstepsolver slave solver type      
   oversample         slave oversample       
   RKorder            slave method order     
   addDelay           add delay              
*/


/* There is 1 text parameter:

   dllName slave dynamic library
*/


/* There are 2 structural parameters:

   v1 number of ports at right
   v2 number of ports at left 
*/


void dyncosimimport00in_(int *n, int *v1, int *v2, double *rp, int *ip
      , char **tp, double *c, void *ps[1])

{
   int loop, error;
/* >>>>>>>>>>>>Extra Initialization Function Declarations Here. */
   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;
   char tmpfullname[1024];
   char *dirsep;
   AME_DLL* amedll=NULL;
/* <<<<<<<<<<<<End of Extra Initialization declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int num_c;   /* Dynamic number of real stores. */
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, addDelay;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   addDelay   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];

   num_c = 1+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;

/*
   If necessary, check values of the following:

   rp[0..2]
*/


/* >>>>>>>>>>>>Initialization Function Check Statements. */

   amedll = calloc(1, sizeof(AME_DLL));
   if (ameload_dll(tp[0], amedll) == 0)
   {
      amefprintf(stderr,"DYNCOSIMIMPORT00> Failed to load \"%s\"\n", tp[0]);
      error = 2;
   }
   else
   {
      /* We need also to verify that the DLL we read is really an AMESim DLL.  If the load_dll
         function have found and set the following pointers we should be OK.

         amedll->AMEInitModel refers to  "AMEInitModel"
         amedll->AMEdoAStep2  refers to "AMEdoAStep2"
         amedll->AMETerminate refers to "AMETerminate"
         amedll->AMEWorkingDir  refers to  "AMEWorkingDir"
      */
      if(amedll->AMEInitModel==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMEInitModel not found\n");
         error = 2;
      }
      if(amedll->AMEdoAStep2==NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMEdoAStep not found\n");
         error = 2;
         }
      if(amedll->AMETerminate==NULL)
         {
         amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMETerminate not found\n");
         error = 2;
         }
      if(amedll->AMESetFinalTime== NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMESetFinalTime not found\n");
         error = 2;
      }
      if(amedll->AMEWorkingDir == NULL)
      {
         amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMEWorkingDir not found\n");
         error = 2;
      }
      if (usefixedstepsolver)
      {
         if((amedll->AMESetUpFixedStepSolver2 == NULL) && (amedll->AMESetUpFixedStepSolver == NULL))
         {
            amefprintf(stderr,"DYNCOSIMIMPORT00> Entrypoint AMESetUpFixedStepSolver not found\n");
            error = 2;
         }
      }
   }

   if (error != 2)
   {
      /* If the DLL is in a diffrent directory we need to call the "AMEWorkingDir" function
         so that the DLL reads the data files from that directory and it writes the result file
         in that directory. We only need to do this when the name of the DLL contains a "/" */
      strcpy(tmpfullname,tp[0]);
      dirsep = strrchr(tmpfullname,'/');
      if(dirsep)
      {
         dirsep++;
         *dirsep = '\0';
         amedll->AMEWorkingDir(tmpfullname);
      }

      /* set slave final time */
      amedll->AMESetFinalTime(getfinaltime_());
   }

   ps[0] = amedll; /* transfer to pointer store */

/* <<<<<<<<<<<<End of Initialization Check Statements. */

/*   Integer parameter checking:   */

   if (errtype < 1 || errtype > 3)
   {
      amefprintf(stderr, "\nerror type must be in range [1..3].\n");
      error = 2;
   }
   if (writelevel < 1 || writelevel > 3)
   {
      amefprintf(stderr, "\nwrite level must be in range [1..3].\n");
      error = 2;
   }
   if (discprintouts < 1 || discprintouts > 2)
   {
      amefprintf(stderr, "\ndiscontinuity printouts must be in range [1..2].\n");
      error = 2;
   }
   if (runstats < 1 || runstats > 2)
   {
      amefprintf(stderr, "\nrunstats must be in range [1..2].\n");
      error = 2;
   }
   if (usefixedstepsolver < 1 || usefixedstepsolver > 2)
   {
      amefprintf(stderr, "\nslave solver type must be in range [1..2].\n");
      error = 2;
   }
   if (oversample < 1 || oversample > 1000000)
   {
      amefprintf(stderr, "\nslave oversample must be in range [1..1000000].\n");
      error = 2;
   }
   if (RKorder < 1 || RKorder > 4)
   {
      amefprintf(stderr, "\nslave method order must be in range [1..4].\n");
      error = 2;
   }
   if (addDelay < 1 || addDelay > 2)
   {
      amefprintf(stderr, "\nadd delay must be in range [1..2].\n");
      error = 2;
   }

/*   Number of stores checking:   */

   if (num_c < 0)
   {
      amefprintf(stderr, "Size of real store array must be positive.\n");
      error = 2;
   }

   if(error == 1)
   {
      amefprintf(stderr, "\nWarning in %s instance %d.\n", _SUBMODELNAME_, *n);
   }
   else if(error == 2)
   {
      amefprintf(stderr, "\nFatal error in %s instance %d.\n", _SUBMODELNAME_, *n);
      amefprintf(stderr, "Terminating the program.\n");
      AmeExit(1);
   }
/* >>>>>>>>>>>>Initialization Function Executable Statements. */
/* <<<<<<<<<<<<End of Initialization Executable Statements. */
}

void dyncosimimport00end_(int *n, int *v1, int *v2, double *rp
      , int *ip, char **tp, double *c, void *ps[1])

{
   int loop, error;
/* >>>>>>>>>>>>Extra Terminate Function Declarations Here. */
/* <<<<<<<<<<<<End of Extra Terminate declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int num_c;   /* Dynamic number of real stores. */
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, addDelay;
   double samptime, printinter, tol;
   char *dllName;

   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   addDelay   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];

   num_c = 1+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   loop = 0;
   error = 0;


/* >>>>>>>>>>>>Terminate Function Executable Statements. */
   {
      AME_DLL *amedll=ps[0];
      amedll->AMETerminate();
      unloadamesimdll(amedll);
   }
   free(ps[0]);
/* <<<<<<<<<<<<End of Terminate Executable Statements. */
}



/*  There are 2 ports.

   Port 1 has 1 variable (port_group = v1):

      1 outputs     output variable [null] basic variable output

   Port 2 has 1 variable (port_group = v2):

      1 inputs     input variable [null] basic variable input
*/

/*  There are 0 internal variables.

*/

void dyncosimimport00_(int *n, int *v1, int *v2, double *outputs
      , double *inputs, double *rp, int *ip, char **tp, double *c
      , void *ps[1], int *flag, double *t)

{
   int loop, logi;
/* >>>>>>>>>>>>Extra Calculation Function Declarations Here. */

   int i, solver_type=0;
   int numinputs_to_submodel = *v2;
   int numoutputs_from_submodel = *v1;
   AME_DLL *amedll=ps[0];
/* <<<<<<<<<<<<End of Extra Calculation declarations. */
   int port_group; /* Sets a number of ports. */
   int outputs_size;
   int inputs_size;
   int num_c;   /* Dynamic number of real stores. */
   int errtype, writelevel, discprintouts, runstats, 
      usefixedstepsolver, oversample, RKorder, addDelay;
   double samptime, printinter, tol;
   char *dllName;

   int run_type = 8;
   
   errtype    = ip[0];
   writelevel = ip[1];
   discprintouts = ip[2];
   runstats   = ip[3];
   usefixedstepsolver = ip[4];
   oversample = ip[5];
   RKorder    = ip[6];
   addDelay   = ip[7];

   samptime   = rp[0];
   printinter = rp[1];
   tol        = rp[2];

   dllName    = tp[0];

   num_c = 1+(*v1);
   port_group = (*v1);
   outputs_size = port_group;
   port_group = (*v2);
   inputs_size = port_group;
   logi = 0;
   loop = 0;

/*
   Set all submodel outputs below:

   *outputs    = ??;
*/



/* >>>>>>>>>>>>Calculation Function Executable Statements. */
   errtype--;
   writelevel--;
   discprintouts--;
   runstats--;
   usefixedstepsolver--;

   /*
      c[0] is the time for the next sample
      c[1] first output
      :
      c[1+v11-1] last output
   */

   if (firstc_())
   {
      c[0] = *t;
   }

   if(firstc_() || (*flag == 0))
   {
      int doastep=0;
      if (firstc_())
      {
         if(ConstructionLevel_() != 0)
         {
            return; /* Do not start if in consolidation step */
         }
			/* Initialize model */
         if (!usefixedstepsolver)
         {
            amedll->AMEInitModel(*t, printinter, samptime,
                                 tol,  errtype, writelevel, discprintouts, runstats, 8, 0,
                                 numinputs_to_submodel, numoutputs_from_submodel, inputs, outputs);
         }
         else
         {
            if(amedll->AMESetUpFixedStepSolver2)
            {
               amedll->AMESetUpFixedStepSolver2(*t, numinputs_to_submodel, numoutputs_from_submodel, run_type,
                                                solver_type, RKorder, samptime/oversample, printinter);
            }
            else
            {
               amedll->AMESetUpFixedStepSolver(*t, numinputs_to_submodel, numoutputs_from_submodel,
                                               solver_type, RKorder, samptime/oversample, printinter);
            }
         }
      }

      if(!isfixedstepsolver_())
      {
         if(*t >= c[0])
         {
            doastep = 1;
         }
      }
      else
      {
         if ( (*t >= c[0]) && !isintermstep_())
         {
            doastep = 1;
         }
      }

      if(doastep)
      {
         /* Time for another step. */
         if (addDelay == 1)
         {
            amedll->AMEdoAStep(*t, numinputs_to_submodel, numoutputs_from_submodel, inputs, outputs);
         }
         else
         {
            amedll->AMEdoAStep2(*t, numinputs_to_submodel, numoutputs_from_submodel, inputs, outputs);
         }

         /* Save the outputs from the other AMESim in real stores
          * so we can set the outputs from this submodel also when
          * we don't call the AMESim model.  */
         for (i=0; i<numoutputs_from_submodel; i++)
         {
            c[i+1] = outputs[i];
         }
         c[0] = c[0] + samptime;
      }
      else
      {
         for (i=0; i<numoutputs_from_submodel; i++)
         {
            outputs[i] = c[i+1];
         }
      }
   }
   else
   {
      /* we must always set the values of a "dynamic"
       * variable - the values are not kept between calls */
      for (i=0; i<numoutputs_from_submodel; i++)
      {
         outputs[i] = c[i+1];
      }
   }

   distim_(&c[0]);

/* <<<<<<<<<<<<End of Calculation Executable Statements. */
}

