#pragma once

#include <enet\enet.h>
#include <conio.h>
#include <string>
#include <memory>

#include "IObserver.h"

class KetchupFactory;
class AmeSHM;

class AmeUdp : public IObserver
{
public:
	AmeUdp(const std::string& addressStr, int listenPort);
	AmeUdp() = default;
	~AmeUdp();
	int send(const void* data, int aSizeOfData, ENetPeer* const aPeer) const;
	ENetPeer* connect(const std::string& aAddress, int aPort) const;
	bool disconnected() const;
	void setFactory(const std::shared_ptr<KetchupFactory>& aFactory);
	void setSharedMemory(std::shared_ptr<AmeSHM> aAmeShm);
	void consumeData();
	void update() override;

private:
	ENetHost* mClient;
    std::shared_ptr<KetchupFactory> mv_spFactory;
	std::shared_ptr<AmeSHM> ameShm;
};