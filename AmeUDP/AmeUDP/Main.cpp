#pragma once
#include <enet\enet.h>
#include <thread>

#include "AmeUdp.h"
#include "AmeSHM.h"
#include "WinSockClient.h"
#include "KetchupFactory.h"


int main(int argc, char** argv)
{
	/*if (argc != 3) {
		printf_s("Please specfy 2 arguments: your IP address and linux IP address!\n");
		return 0;
	}
	*/

	//shm sharedptr
	//make shared everywhere
	//breakpoint update
	std::shared_ptr<AmeUdp> udp;
	std::shared_ptr<KetchupFactory> factory;
	std::shared_ptr<AmeSHM> shm = std::make_shared<AmeSHM>("shm_0");
	factory = std::make_shared<KetchupFactory>(shm/*, &udp*/);
	udp = std::make_shared<AmeUdp>("127.0.0.1" /*(std::string)argv[1]*/, 980);
	udp.get()->setSharedMemory(shm);

	shm->SetFactory(factory);
	udp.get()->setFactory(factory);

	factory.get()->setClient(udp);

	factory->attach(udp);
	factory->attach(shm);
	if (factory.get()->establishConnection((std::string)"127.0.0.1"))//(std::string)argv[2]))
	{
		std::thread producerThread(&AmeSHM::StartExchange, shm);
		std::thread consumerThread(&AmeUdp::consumeData, udp);
		producerThread.join();
		consumerThread.join();
	}

	return 0;
}


	/*if (argc < 4)
	{
		printf("Not enought arguments!\n");
		printf("Please set: Source IP, Source Port, Destination IP, Destination Port!\n");
		return 0;
	}*/



	//bool running = true;
	//int counter = 0;
	//
	//AmeUdp client("127.0.0.1"/*argv[0]*/, 980/*(int)argv[1]*/);
	//ENetPeer* peer;
	//peer = client.connect("127.0.0.1"/*argv[2]*/,981/*(int)argv[3]*/);
	//data a[8] = { 1.821688e-43,-2.2,3,4,5,6,7,8};
	//while (running) {
	//	if (!client.disconnected())
	//	{
	//		a[0] = a[0] + 0.000000000001;
	//		a[1] += 0.01;
	//		a[2] += 0.21;
	//		a[3] += 0.61;
	//		a[5] += 0.21;
	//		if(client.send((void*)a, (int)(sizeof(a[0]) * 8 + 1), peer) != 0)
	//		{
	//			running = false;
	//			printf("Message failed to send!");
	//		}
	//		else
	//		{
	//			counter++;
	//		}
	//		printf("Message succesfully sent:%d \n", counter);
	//	}
	//	else
	//	{
	//		printf("You have been disconnected!");
	//		running = false;
	//	}
	//}
	//return 0;
	//}

	//execute from cmd and -winsock or ENET option 
	//check it to be optimal
	// to do:
	// make mClient unique  
	// make mPeer unique

	//try
	//{
	//	float buf[8] = { 3,6,2,12.6,5.22,0,0 };
	//	int i = 0;
	//	WinSockClient winSockClient("127.0.0.1"/*"192.168.1.100"*/, 981, "127.0.0.1"/*"192.168.1.100"*/, 980);
	//	winSockClient.bindSock();
	//	winSockClient.connectToServer();
	//	while (i < 10000000)
	//	{
	//		buf[0] += 0.01;
	//	winSockClient.send(*buf);
	//	i++;
	//	}

	//}
	//catch (...)
	//{
	//	std::cout << "Execution ended!" << std::endl;
	//	/*std::cerr << e.what() << std::endl;*/
	//}

	//return 0;
//}




//#include <boost/lockfree/spsc_queue.hpp>
//#include <thread>
//#include <iostream>
//
//boost::lockfree::spsc_queue<int> q{ 100 };
//int sum = 0;
//
//void produce()
//{
//	for (int i = 1; i <= 100; ++i)
//		q.push(i);
//}
//
//void consume()
//{
//	int i;
//	while (q.pop(i))
//		sum += i;
//}
//
//int main()
//{
//	std::thread t1{ produce };
//	std::thread t2{ consume };
//	t1.join();
//	t2.join();
//	consume();
//	std::cout << sum << '\n';
//}
