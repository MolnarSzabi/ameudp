#pragma once
#include <vector>
#include <memory>

class IObserver;
class Observable 
{
public:
	void attach(std::shared_ptr<IObserver> aObserver);
	void detach(std::shared_ptr<IObserver> aObserver);
	void notifySubscribers();
protected:
	std::vector<std::shared_ptr<IObserver>> mv_vecSubscribers;
};

