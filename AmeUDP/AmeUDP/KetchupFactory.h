#pragma once
#include <atomic>
#include <deque>
#include <array>
#include <string>
#include <memory>

#include <enet/enet.h>
#include "AmeUdp.h"
#include "Observable.h"

///   consumer-producer lock-free circular deque
class AmeSHM;
typedef float _Float32;
constexpr int gcBufferLength = 2;

class KetchupFactory: public Observable
{
public:
	KetchupFactory(std::shared_ptr<AmeSHM> aCustomer/*, const std::shared_ptr<AmeUdp>* aClient*/);
	void setClient(const std::shared_ptr<AmeUdp>& aClient);
	void produce(const std::array<float, 8>& aKetchupBox);
	void consume();
	void depositIntoBox();
	int  increment(const std::atomic<int>& atomic_aPosition) const;
	bool establishConnection(std::string& aUnixAddress);
	bool tryProduce(const std::array<float,8>& data);
	bool tryConsume();
	void exportKetchup();


private:
	std::atomic<int> mv_atomicTransport;
	std::atomic<int> mv_atomicDeposit;
	std::array<std::array<float, 8>, gcBufferLength> mv_vecKetchupDeposit;
	_Float32 mv_vecKetchupBox[8];

private:
	std::shared_ptr<AmeSHM> mv_pCustomer;
	std::shared_ptr<AmeUdp> mv_spClient;
	ENetPeer* mv_pPeer;
};