#include "KetchupFactory.h"
#include "AmeSHM.h"

#include <algorithm>

bool KetchupFactory::establishConnection(std::string& aUnixAddress)
{
	mv_pPeer = mv_spClient.get()->connect(aUnixAddress/*argv[2]*/,981/*(int)argv[3]*/);
	if (mv_pPeer == nullptr)
		return false;
	
	return true;
}

void KetchupFactory::exportKetchup()
{
	if (!mv_spClient.get()->disconnected()) {
		depositIntoBox();
		if (mv_spClient.get()->send((void*)mv_vecKetchupBox, (int)(sizeof(mv_vecKetchupBox[0]) * 8 + 1), mv_pPeer) != 0)
		{
			printf("Failed to send package! \n You have been disconnected!\n");
			//Disconnect(mv_pCustomer);
			notifySubscribers();
		}
	}
	else printf("You have been disconnected!\n (Client Disconnected)\n");
}


KetchupFactory::KetchupFactory(std::shared_ptr<AmeSHM> aCustomer/*,const std::shared_ptr<AmeUdp>* aClient*/)
	/*:client(*aClient),*/
	:mv_pCustomer(aCustomer),
	mv_pPeer(nullptr)
{
	//if (!establishConnection())
	//	std::exit(EXIT_FAILURE);

	std::fill(std::begin(mv_vecKetchupBox), std::end(mv_vecKetchupBox), 0);
	std::array<float, 8> ketchup;
	ketchup.fill(0);
	std::fill(std::begin(mv_vecKetchupDeposit), std::end(mv_vecKetchupDeposit), ketchup);
	mv_atomicDeposit.store(0);
	mv_atomicTransport.store(0);
}

void KetchupFactory::setClient(const std::shared_ptr<AmeUdp>& aClient)
{
	mv_spClient = aClient;
}

void KetchupFactory::produce(const std::array<float, 8>& data)
{
	while (!tryProduce(data));
}

void KetchupFactory::consume()
{
	while (!tryConsume());
}

void KetchupFactory::depositIntoBox()
{
	const auto currentHead = mv_atomicTransport.load();
	std::copy(std::begin(mv_vecKetchupDeposit[currentHead]), std::end(mv_vecKetchupDeposit[currentHead]), std::begin(mv_vecKetchupBox));
}

int KetchupFactory::increment(const std::atomic<int>& position) const
{
	return (position.load() + 1) % gcBufferLength;
}

bool KetchupFactory::tryProduce(const std::array<float, 8>& data)
{
	const auto currentTail = mv_atomicDeposit.load();
	const auto nextTail = increment(currentTail);
	const auto currentHead = mv_atomicTransport.load();

	if (nextTail != currentHead) {  
		mv_vecKetchupDeposit[currentTail] = data;
		mv_atomicDeposit.store(nextTail);
		return true;
	}

	return false;
}

bool KetchupFactory::tryConsume()
{
	auto const currentHead = mv_atomicTransport.load();
	auto const currentTail = mv_atomicDeposit.load();
	if (currentHead == currentTail)
	{
		return false;
	}

	exportKetchup();
	auto const nextHead = increment(currentHead);
	mv_atomicTransport.store(nextHead);
	
	return true;
}

