#pragma once
#include <vector>
class IObserver
{
public:
	virtual void update() = 0;
	virtual ~IObserver() = default;
};