#include "WinSockClient.h"

WinSockClient::WinSockClient(const char* ipAddrServer, int portServer, const char* ipAddrClient, int portClient) :
	ipAddrServer(ipAddrServer),
	portServer(portServer),
	ipAddrClient(ipAddrClient),
	portClient(portClient),
	listenSocket(INVALID_SOCKET),
	sendSocket(INVALID_SOCKET)
{
	init();
	createSocketListen();
	createSocketSend();
	createClient();
	createServer();
}

WinSockClient::~WinSockClient()
{
	closesocket(sendSocket);
	closesocket(listenSocket);
	WSACleanup();

}

void WinSockClient::createClient()
{
	client.sin_family = AF_INET;
	client.sin_port = htons(portClient);
	inet_pton(AF_INET, ipAddrClient, &client.sin_addr);
}


int WinSockClient::bindSock()
{
	int result = 0;

	result = bind(listenSocket, (SOCKADDR*)&client, sizeof(client));
	if (result == SOCKET_ERROR)
	{
		std::cout << "Bind failed!" << std::endl;
		throw "Bind failed!";
	}

	std::cout << "Binding completed successfully!" << std::endl;
	
	return result;
}

int WinSockClient::init()
{
	WSADATA data;
	WORD version = MAKEWORD(2, 2);
	int windowsSockOK = WSAStartup(version, &data);

	if (windowsSockOK != 0)
	{
		std::cout << "Can't start WinSock!\n"
			<< "\n Socket initialization failed!\n";
		throw "Init failed!";
	}

	return windowsSockOK;
}

void WinSockClient::createServer()
{
	server.sin_family = AF_INET;
	server.sin_port = htons(portServer);
	inet_pton(AF_INET, ipAddrServer, &server.sin_addr);
}

int WinSockClient::connectToServer()
{

	int result = connect(listenSocket, (SOCKADDR*)&server, sizeof(server)); //connect listenport with server 

	if (result != 0)
	{
		std::cout << "Connect failed!" << "\n";
		throw "Connect failed!";

	}

	std::cout << "Connection succeeded!" << "\n";
	return result;
}

int WinSockClient::createSocketSend()
{
	sendSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (sendSocket == INVALID_SOCKET)
	{
		std::cout << "Send socket failed to initialise! " << WSAGetLastError() << std::endl;
		throw "Send socket failed to initialise!";
	}

	return 1;
}

int WinSockClient::createSocketListen()
{
	listenSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (listenSocket == INVALID_SOCKET)
	{
		std::cout << "Listen socket failed to initialise! " << WSAGetLastError() << std::endl;
		throw "Listen socket failed to initialise!";
	}

	return 1;
}

int WinSockClient::send(const float& buf)
{

	int sendOK = sendto(listenSocket, (char*)&buf, sizeof((&buf)[0])*8 + 1, 0, (sockaddr*)&server, sizeof(server));
	if (sendOK == SOCKET_ERROR)
	{
		std::cout << "Package failed to send!" << WSAGetLastError() << std::endl;
		throw "Package failed to send!";
	}
	return sendOK;
}

