#pragma once
#include <string>
#include "KetchupFactory.h"
#include <memory>

#include "IObserver.h"

class AmeSHM : public std::enable_shared_from_this<AmeSHM>, public IObserver
{
public:
	AmeSHM() = default;
	AmeSHM(const std::string& aMemoryName);
	~AmeSHM();

	void SetFactory(std::shared_ptr<KetchupFactory> aFactory);
	void StartExchange();
	friend void Disconnect(std::shared_ptr<AmeSHM> aCustomer);
	friend bool isConnected(std::shared_ptr<AmeSHM> aCustomer);
	void update() override;
private:
	void ExchangeData();
	void PrintOutput() const;

	static constexpr int gc_nIsMaster = 1;
	static constexpr int gc_nDataAttrCount = 10;
	const std::string mc_szMemoryName;

	bool mv_bConnectionEstablished;
	double* mv_vecSendData, *mv_vecReceiveData;
	double mv_currentTime = 0.0;
	std::shared_ptr<KetchupFactory> mv_spFactory;
	std::array<float, 8> mv_vecMoogData;
};

