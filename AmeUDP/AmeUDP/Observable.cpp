#include "Observable.h"
#include "IObserver.h"

void Observable::attach(std::shared_ptr<IObserver> aObserver)
{
	mv_vecSubscribers.push_back(aObserver);
}

void Observable::detach(std::shared_ptr<IObserver> aObserver)
{
	auto it = std::find(std::begin(mv_vecSubscribers), std::end(mv_vecSubscribers), aObserver);
	if(it != std::end(mv_vecSubscribers))
		mv_vecSubscribers.erase(it);
}

void Observable::notifySubscribers()
{
	for (auto elem : mv_vecSubscribers)
	{
		elem->update();
	}
}
