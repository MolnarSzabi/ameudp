#include "AmeSHM.h"
#include <algorithm>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <chrono>

extern "C"
{
#include <amecommunication_shm.h>
}


AmeSHM::AmeSHM(const std::string& memoryName):
	mc_szMemoryName(memoryName),
	mv_bConnectionEstablished(false)
{
	mv_vecMoogData.fill(0);
	mv_vecSendData = (double*)calloc(gc_nDataAttrCount, sizeof(double));
	mv_vecReceiveData = (double*)calloc(gc_nDataAttrCount, sizeof(double));
}

void AmeSHM::StartExchange()
{
	ameshm_conn_attributes lv_shmConnAttr;

	try
	{
		if (ameshm_init(&lv_shmConnAttr, gc_nIsMaster, _strdup(mc_szMemoryName.c_str()), gc_nDataAttrCount, gc_nDataAttrCount) == SUCCESS)
		{
			mv_bConnectionEstablished = true;
			printf_s("Connection established successfully!\n");
			auto start = std::chrono::high_resolution_clock::now();
			while (mv_bConnectionEstablished)
			{
				auto lv_nReturnCode = ameshm_exchange(&lv_shmConnAttr, mv_vecSendData, mv_vecReceiveData);
				switch (lv_nReturnCode)
				{
					case SUCCESS:
					{
						ExchangeData();
						break;
					}
					case CONNECTION_SHUTDOWN:
					{
						printf_s("Connection ended peacefully!\n");
						mv_spFactory->notifySubscribers();
						//Disconnect(this);
						auto end = std::chrono::high_resolution_clock::now();
						auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
						printf_s("Simulation took %I64d seconds\n",microseconds.count() / 1000000);

						ameshm_close(&lv_shmConnAttr);
						mv_bConnectionEstablished = false;
						break;
					}
					case BAD_ARGUMENT:
					{
						printf("Ok, what the hell?\n");
						break;
					}
					case UNKNOWN_ERROR:
					{
						printf("It's unknown error bro.");
						break;
					}

					default:
					{
						printf_s("You've done fucked up!\n");
						mv_bConnectionEstablished = false;
						ameshm_close(&lv_shmConnAttr);
					}
				}
			}
		}
		else
		{
			printf_s("Socket initialization failed. Check arguments!\n");
		}
	}
	catch (...)
	{
		printf_s("You forgot to start up the server, you dense motherfucker!\n");
	}
}


void AmeSHM::SetFactory(std::shared_ptr<KetchupFactory> aFactory)
{
	mv_spFactory = aFactory;
}

void AmeSHM::update()
{
	Disconnect(shared_from_this());
}

void AmeSHM::ExchangeData()
{
	mv_vecSendData[0] = mv_currentTime + 0.001;
	mv_vecSendData[1] = mv_currentTime;
	
	std::copy(mv_vecReceiveData + 2, mv_vecReceiveData + 10, mv_vecSendData + 2);  
	std::transform(mv_vecSendData + 2, mv_vecSendData + 10, std::begin(mv_vecMoogData),  [](double doubleData) -> float {return float(doubleData); });
	
	mv_currentTime += 0.001;

	mv_spFactory.get()->produce(mv_vecMoogData);

	//PrintOutput();
}

void AmeSHM::PrintOutput() const
{
	printf_s("%lf, Send data:%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf%lf \n", mv_vecReceiveData[0], mv_vecSendData[2], mv_vecSendData[3], mv_vecSendData[4], mv_vecSendData[5], mv_vecSendData[6], mv_vecSendData[7], mv_vecSendData[8], mv_vecSendData[9], mv_vecSendData[10]);
	//printf_s("%lf, %lf, %lf, %lf, Send data:%lf\n", mv_vecReceiveData[0], mv_vecReceiveData[1], mv_vecReceiveData[2], mv_vecReceiveData[3], mv_vecSendData[2]);
}


AmeSHM::~AmeSHM()
{
	free(mv_vecSendData);
	free(mv_vecReceiveData);
}

void Disconnect(std::shared_ptr<AmeSHM> aCustomer)
{
	//if (isConnected(aCustomer))
		aCustomer->mv_bConnectionEstablished = false;
}

bool isConnected(std::shared_ptr<AmeSHM> aCustomer)
{
	if (aCustomer->mv_bConnectionEstablished)
		return true;

	return false;
}
