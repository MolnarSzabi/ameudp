#pragma once

#include <WS2tcpip.h>
#include <iostream>

typedef float data;
class WinSockClient
{
public:
	WinSockClient(const char* ipAddrServer, int portServer, const char* ipAddrClient, int portClient);
	~WinSockClient();
	int bindSock();
	int connectToServer();
	int send(const float& buf);

private:
	int init();
	int createSocketSend();
	int createSocketListen();
	void createClient();
	void createServer();

private:
	sockaddr_in server;
	sockaddr_in client;
	SOCKET sendSocket;
	SOCKET listenSocket;

private:
	const char* ipAddrServer;
	int portServer;
	const char* ipAddrClient;
	int portClient;

};

