#include "AmeUdp.h"
#include "KetchupFactory.h"
#include "AmeSHM.h"

AmeUdp::AmeUdp(const std::string& addressStr, int listenPort) :
	mClient(nullptr),
	ameShm(nullptr),
	mv_spFactory(nullptr)
{
	ENetAddress address;

	if (enet_initialize() != 0) {
		fprintf(stderr, "An error occurred while initializing ENet.\n");
		return;
	}

	enet_address_set_host(&address, addressStr.data());

	address.port = listenPort;
	mClient = enet_host_create(  
		&address /* the address to bind the server host to */,
		1      /* allow up to 32 clients and/or outgoing connections */,
		0,     /* channel limit, 0 = ENET_PROTOCOL_MAXIMUM_CHANNEL_COUNT */
		0      /* assume any amount of incoming bandwidth */,
		0      /* assume any amount of outgoing bandwidth */);


	if (!mClient) {
		printf("An error occurred while trying to create an ENet client host.\n");
		return;
	}

}

AmeUdp::~AmeUdp()
{
	if (mClient)
		enet_host_destroy(mClient);
	enet_deinitialize();
}


int AmeUdp::send(const void* data, int len, ENetPeer* const peer) const  
{
	ENetPacket* packet = enet_packet_create(data, len, ENET_PACKET_FLAG_RELIABLE);
	int res = enet_peer_send(peer, 0, packet);
	//enet_host_flush(mClient); //only be used in circumstances where one wishes to send queued packets
	return res;
}

ENetPeer* AmeUdp::connect(const std::string& addressStr, int port) const
{
	ENetAddress serverAddress;
	ENetPeer* peer;
	ENetEvent  event;

	enet_address_set_host(&serverAddress, addressStr.data());
	serverAddress.port = port;

	peer = enet_host_connect(
		mClient,			//host seeking the connection
		&serverAddress,		//destination
		0,					//channels to allocate
		0					//user data supplied to recieving host
	);

	if (peer == nullptr) {
		printf("Could not connect to server\n");
		return 0;
	}

	if (enet_host_service(mClient, &event, 1000) > 0 &&
		event.type == ENET_EVENT_TYPE_CONNECT) {
		printf("Connection to %s succeeded.\n", addressStr.data());

	}
	else {
		enet_peer_reset(peer);
		printf("Could not connect to %s\n", addressStr.data());
		return 0;
	}

	return peer;
}

bool AmeUdp::disconnected() const
{
	ENetEvent event;

	if (enet_host_service(mClient, &event, 1) > 0) {
		if (event.type == ENET_EVENT_TYPE_DISCONNECT)
			return true;
	}

	return false;
}

void AmeUdp::setFactory(const std::shared_ptr<KetchupFactory>& aFactory)
{
	mv_spFactory = aFactory;
}

void AmeUdp::setSharedMemory(std::shared_ptr<AmeSHM> aAmeShm)
{
	ameShm = aAmeShm;
}

void AmeUdp::consumeData()
{
	while (!isConnected(ameShm)); //wait for connection

	while(isConnected(ameShm))
		mv_spFactory.get()->consume();
}

void AmeUdp::update()
{
	Disconnect(this->ameShm);
}
